title: Testletter 1
opening: Hallo ${current.firstName.value},
closing: Gruß,
signature: Ekkart.
filename:	${current.fileName.value}.md
outputpath: Briefe/temp

einleitender Text, sichtbar für alle.

# Sichtbar für alle

Weitere Informationen, die für alle wichtig sind.

Abschließende Bemerkungen vor der Schlussformel.
