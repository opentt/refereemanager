title: Testletter 2
opening: Hallo ${current.firstName.value},
closing: Gruß,
signature: Ekkart.
attachment: /home/ekleinod/working/git/open-tt/refereemanager/refereemanager/src/test/data/one_page_landscape.pdf :: One fine page :: true
filename:	${current.fileName.value}.md
outputpath: Briefe/temp

einleitender Text, sichtbar für alle.

# Sichtbar für alle

Weitere Informationen, die für alle wichtig sind.

Abschließende Bemerkungen vor der Schlussformel.
