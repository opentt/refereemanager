title:	Datenblatt
subtitle:	${current.displayTitle.value}
date:			Stand: <#setting locale="de"> ${refereeManagerData.info.modified?datetime.iso?string["d. MMMM yyyy"]}
filename:	${current.fileName.value}.md
options:	notitlepage, noauthor, noemail, nosponsorlogo, pagestyle=leer
outputpath: /home/ekleinod/working/git/open-tt/refereemanager/refereemanager/src/test/data/output/documents

<#setting locale="de">

```{=latex}
	{
		\footnotesize
		\sffamily

		\begin{longtabu}[l]{@{}>{\scriptsize\bfseries}p{.23\textwidth}@{\hspace{.02\textwidth}}p{.75\textwidth}@{}}\savetabu{tablayout}
```
`\multicolumn{2}{@{}l@{}}{`{=latex}**Sichtbar für Click-TT, Vereine, DTTB, andere VSR, VSRA**`}\\`{=latex}
```{=latex}
			\midrule
```

Name `&`{=latex} ${current.displayTitle.value}`\\`{=latex}
Ausbildung `&`{=latex} ${current.highestTrainingLevel.type.shorttitle.value} (${current.highestTrainingLevel.type.title.value})`\\`{=latex}
Mitglied bei `&`{=latex} <#if current.member?? >${current.member.displayText.value}<#else>---</#if>`\\`{=latex}
<#if current.reffor??>Schiedst für `&`{=latex} ${current.reffor.displayText.value}`\\`{=latex}</#if>

```{=latex}
			\bottomrule
		\end{longtabu}
```

```{=latex}
		\begin{longtabu}[l]{\preamble{tablayout}}
```
`\multicolumn{2}{@{}l@{}}{`{=latex}**Sichtbar für Vereine, DTTB, andere VSR, VSRA**`}\\`{=latex}
```{=latex}
			\midrule
```

E-Mail `&`{=latex} <#list current.EMail as email>${email.displayText.value}<#sep>; </#sep><#else>---</#list>`\\`{=latex}
Telefon `&`{=latex} <#list current.phoneNumber as phonenumber>${phonenumber.displayText.value}<#sep>; </#sep><#else>---</#list>`\\`{=latex}
Geschlecht `&`{=latex} ${current.sexType.displayText.value}`\\`{=latex}

```{=latex}
			\bottomrule
		\end{longtabu}
```

```{=latex}
		\begin{longtabu}[l]{\preamble{tablayout}}
```
`\multicolumn{2}{@{}l@{}}{`{=latex}**Sichtbar für DTTB, VSRA**`}\\`{=latex}
```{=latex}
			\midrule
```

Geburtstag `&`{=latex} <#if current.birthday?? >${current.birthday.value?date['yyyy-MM-dd']?string['dd.MM.yyyy']}<#else>---</#if>`\\`{=latex}

```{=latex}
			\bottomrule
		\end{longtabu}
```

```{=latex}
		\begin{longtabu}[l]{\preamble{tablayout}}
```
`\multicolumn{2}{@{}l@{}}{`{=latex}**Sichtbar für andere VSR, VSRA**`}\\`{=latex}
```{=latex}
			\midrule
```

Status `&`{=latex} ${current.status.title.value}`\\`{=latex}
Nächste Fortbildung `&`{=latex} <#if current.nextTrainingUpdate?? >${current.nextTrainingUpdate.value?date['yyyy-MM-dd']?string['yyyy']}</#if>`\\`{=latex}

```{=latex}
			\bottomrule
		\end{longtabu}
```

```{=latex}
		\begin{longtabu}[l]{\preamble{tablayout}}
```
`\multicolumn{2}{@{}l@{}}{`{=latex}**Sichtbar für VSRA**`}\\`{=latex}
```{=latex}
			\midrule
```

VSR-Mitteilungen `&`{=latex} <#if current.EMail?? && (current.EMail?size > 0) >per E-Mail<#if current.docsByLetterCombined.value> und </#if></#if><#if current.docsByLetterCombined.value>per Brief</#if>`\\`{=latex}
Adresse `&`{=latex} <#list current.address as address>${address.displayText.value}<#sep>; </#sep><#else>---</#list>`\\`{=latex}
URL `&`{=latex} <#list current.URL as url>${url.displayText.value}<#sep>; </#sep><#else>---</#list>`\\`{=latex}
Bevorzugt schiedsen `&`{=latex} <#list current.prefer as prefer>${prefer.displayText.value}<#sep>; </#sep><#else>---</#list>`\\`{=latex}
Nicht schiedsen `&`{=latex} <#list current.avoid as avoid>${avoid.displayText.value}<#sep>; </#sep><#else>---</#list>`\\`{=latex}
<#list current.trainingLevel as traininglevel>
${traininglevel.type.displayText.value} seit `&`{=latex} <#if traininglevel.since?? >${traininglevel.since.value?date['yyyy-MM-dd']?string['dd.MM.yyyy']}<#else>---</#if>`\\`{=latex}
</#list>
Letzte Fortbildung `&`{=latex} <#if current.lastTrainingUpdate?? >${current.lastTrainingUpdate.value?date['yyyy-MM-dd']?string['dd.MM.yyyy']}</#if>`\\`{=latex}
<#if current.remark??>Anmerkung `&`{=latex} ${current.remark.value}`\\`{=latex}</#if>

```{=latex}
			\bottomrule
		\end{longtabu}
	}
```
