title:	Kontaktliste Verbandsschiedsrichter
subtitle:	BTTV, Saison ${refereeManagerData.content.season.title.value}
options:	noemail
date:			Stand: <#setting locale="de"> ${refereeManagerData.info.modified?datetime.iso?string["d. MMMM yyyy"]}
filename:	${refereeManagerData.content.season.title.value}_BeTTV_VSR-Kontaktliste.md
outputpath: /home/ekleinod/working/git/open-tt/refereemanager/refereemanager/src/test/data/output/document

```{=latex}
\footnotesize

\tabulinesep=_.5\parskip^.5\parskip

\begin{longtabu}[l]{@{}>{\RaggedRight}p{.25\textwidth}@{\hspace{.01\textwidth}}>{\RaggedRight}p{.41\textwidth}@{\hspace{.01\textwidth}}>{\RaggedRight}p{.24\textwidth}@{\hspace{.01\textwidth}}>{\RaggedRight}p{.07\textwidth}@{}}
		\toprule
```
`{\scriptsize`{=latex}**Name**`} & {\scriptsize`{=latex}**Kontakt**`} & {\scriptsize`{=latex}**Mitglied**`\newline `{=latex}***Schiedst für***`} & {\scriptsize`{=latex}**Ausb.**`}\\`{=latex}
```{=latex}
		\midrule
	\endhead
		\midrule
```
`\multicolumn{4}{@{}r@{}}{{\scriptsize`{=latex}*weiter auf der nächsten Seite...*`}}\\`{=latex}
```{=latex}
		\bottomrule
	\endfoot
		\bottomrule
	\endlastfoot
```

<#list selection as referee>
<@compress single_line=true>
<#if referee?item_parity == "odd" >`\rowcolor{Linen}`{=latex}<#else>`\rowcolor{white}`{=latex}</#if>
<#if referee.status.mmdmarkupstart??>${referee.status.mmdmarkupstart.value}</#if>${referee.tableName.value}<#if referee.status.mmdmarkupend??>${referee.status.mmdmarkupend.value}</#if>
`&`{=latex}
<#list referee.phoneNumber as phonenumber><#if referee.status.mmdmarkupstart??>${referee.status.mmdmarkupstart.value}</#if>${phonenumber.displayText.value}<#if referee.status.mmdmarkupend??>${referee.status.mmdmarkupend.value}</#if><#sep>`\newline `{=latex}</#sep></#list><#if referee.phoneNumber?? && (referee.phoneNumber?size > 0) && referee.EMail?? && (referee.EMail?size > 0) >`\mbox{}\newline `{=latex}</#if><#list referee.EMail as email><#if referee.status.mmdmarkupstart??>${referee.status.mmdmarkupstart.value}</#if>${email.displayText.value}<#if referee.status.mmdmarkupend??>${referee.status.mmdmarkupend.value}</#if><#sep>`\newline `{=latex}</#sep></#list>
`&`{=latex}
<#if referee.member??><#if referee.status.mmdmarkupstart??>${referee.status.mmdmarkupstart.value}</#if>${referee.member.displayTitleShort.value}<#if referee.status.mmdmarkupend??>${referee.status.mmdmarkupend.value}</#if></#if><#if referee.reffor??>`\newline `{=latex}*<#if referee.status.mmdmarkupstart??>${referee.status.mmdmarkupstart.value}</#if>${referee.reffor.displayTitleShort.value}<#if referee.status.mmdmarkupend??>${referee.status.mmdmarkupend.value}</#if>*</#if>
`&`{=latex}
<#if referee.status.mmdmarkupstart??>${referee.status.mmdmarkupstart.value}</#if>${referee.highestTrainingLevel.type.shorttitle.value}<#if referee.status.mmdmarkupend??>${referee.status.mmdmarkupend.value}</#if>
`\\`{=latex}
</@compress>

</#list>

```{=latex}
\end{longtabu}
```

**Legende:**

<#list refereeManagerData.content.statusType as statustype>
- <#if statustype.mmdmarkupstart??>${statustype.mmdmarkupstart.value}</#if><#if statustype.remark??>${statustype.remark.value}<#else>${statustype.title.value}</#if><#if statustype.mmdmarkupend??>${statustype.mmdmarkupend.value}</#if>
</#list>
