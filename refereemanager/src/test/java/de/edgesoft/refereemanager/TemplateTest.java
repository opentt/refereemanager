package de.edgesoft.refereemanager;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import de.edgesoft.edgeutils.freemarker.TemplateHandler;
import de.edgesoft.refereemanager.utils.DocumentGenerationData;
import freemarker.template.Template;

/**
 * Unit test for templates
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.17.0
 */
public class TemplateTest {

	private static DocumentGenerationData theTestData;

	/**
	 * Definition of test arguments.
	 *
	 * @return stream of arguments
	 */
	private static Stream<Arguments> createArgumentsFillTemplate() {

		return Stream.of(

				Arguments.of("today is today", "today is today")

				);

	}

	/**
	 * Initializes test data.
	 */
	@BeforeAll
	private static void initTestData() {

		theTestData = DocumentGenerationData.createInstance();

	}

	/**
	 * Tests method.
	 *
	 * @param theTemplateContent birthday
	 * @param theResult date to compare to
	 */
	@ParameterizedTest(name = "{index} ==> {0}")
	@MethodSource("createArgumentsFillTemplate")
	public static void testGetDisplayAge(final String theTemplateContent, final String theResult) {

		String sResult = null;

		try {
			Template tplTemp = new Template(null, new StringReader(theTemplateContent), TemplateHandler.getConfiguration());
			try (StringWriter wrtContent = new StringWriter()) {
				tplTemp.process(theTestData, wrtContent);
				sResult = wrtContent.toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		assertEquals(theResult, sResult);

	}

}

/* EOF */
