
package de.edgesoft.refereemanager.jaxb;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import de.edgesoft.refereemanager.model.EventDateModel;
import de.edgesoft.refereemanager.model.URLModel;


/**
 * <p>Java class for Tournament complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tournament"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{}EventDate"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="announcement_u_r_l" type="{}URL" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="information_u_r_l" type="{}URL" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="result_u_r_l" type="{}URL" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="organizing_club" type="{http://www.w3.org/2001/XMLSchema}IDREF" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tournament", propOrder = {
    "announcementURL",
    "informationURL",
    "resultURL",
    "organizingClub"
})
public class Tournament
    extends EventDateModel
{

    @XmlElement(name = "announcement_u_r_l", type = URLModel.class)
    protected List<URL> announcementURL;
    @XmlElement(name = "information_u_r_l", type = URLModel.class)
    protected List<URL> informationURL;
    @XmlElement(name = "result_u_r_l", type = URLModel.class)
    protected List<URL> resultURL;
    @XmlElement(name = "organizing_club", type = Object.class)
    @XmlIDREF
    @XmlSchemaType(name = "IDREF")
    protected Club organizingClub;

    /**
     * Gets the value of the announcementURL property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the announcementURL property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAnnouncementURL().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link URL }
     * 
     * 
     */
    public List<URL> getAnnouncementURL() {
        if (announcementURL == null) {
            announcementURL = new ArrayList<URL>();
        }
        return this.announcementURL;
    }

    /**
     * Gets the value of the informationURL property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the informationURL property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInformationURL().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link URL }
     * 
     * 
     */
    public List<URL> getInformationURL() {
        if (informationURL == null) {
            informationURL = new ArrayList<URL>();
        }
        return this.informationURL;
    }

    /**
     * Gets the value of the resultURL property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resultURL property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResultURL().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link URL }
     * 
     * 
     */
    public List<URL> getResultURL() {
        if (resultURL == null) {
            resultURL = new ArrayList<URL>();
        }
        return this.resultURL;
    }

    /**
     * Gets the value of the organizingClub property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Club getOrganizingClub() {
        return organizingClub;
    }

    /**
     * Sets the value of the organizingClub property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setOrganizingClub(Club value) {
        this.organizingClub = value;
    }

}
