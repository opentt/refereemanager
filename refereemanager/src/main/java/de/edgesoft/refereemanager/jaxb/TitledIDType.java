
package de.edgesoft.refereemanager.jaxb;

import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import de.edgesoft.edgeutils.commons.IDType;
import de.edgesoft.edgeutils.javafx.SimpleStringPropertyAdapter;
import de.edgesoft.refereemanager.model.PersonModel;
import de.edgesoft.refereemanager.model.SeasonModel;
import de.edgesoft.refereemanager.model.StatusTypeModel;
import de.edgesoft.refereemanager.model.TrainingLevelTypeModel;
import de.edgesoft.refereemanager.model.VenueModel;


/**
 * <p>Java class for TitledIDType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TitledIDType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{}IDType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="title" type="{}StringProperty" minOccurs="0"/&gt;
 *         &lt;element name="shorttitle" type="{}StringProperty" minOccurs="0"/&gt;
 *         &lt;element name="remark" type="{}StringProperty" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TitledIDType", propOrder = {
    "title",
    "shorttitle",
    "remark"
})
@XmlSeeAlso({
    SeasonModel.class,
    PersonModel.class,
    VenueModel.class,
    PersonVenueReferrer.class,
    EventDateType.class,
    SexType.class,
    RoleType.class,
    ContactType.class,
    RefereeAssignmentType.class,
    StatusTypeModel.class,
    TrainingLevelTypeModel.class
})
public abstract class TitledIDType
    extends IDType
{

    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(SimpleStringPropertyAdapter.class)
    protected SimpleStringProperty title;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(SimpleStringPropertyAdapter.class)
    protected SimpleStringProperty shorttitle;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(SimpleStringPropertyAdapter.class)
    protected SimpleStringProperty remark;

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public SimpleStringProperty getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(SimpleStringProperty value) {
        this.title = value;
    }

    /**
     * Gets the value of the shorttitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public SimpleStringProperty getShorttitle() {
        return shorttitle;
    }

    /**
     * Sets the value of the shorttitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShorttitle(SimpleStringProperty value) {
        this.shorttitle = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public SimpleStringProperty getRemark() {
        return remark;
    }

    /**
     * Sets the value of the remark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemark(SimpleStringProperty value) {
        this.remark = value;
    }

}
