
package de.edgesoft.refereemanager.jaxb;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import de.edgesoft.edgeutils.commons.AbstractModelClass;
import de.edgesoft.edgeutils.javafx.SimpleObjectPropertyLocalDateAdapter;
import de.edgesoft.edgeutils.javafx.SimpleObjectPropertyLocalTimeAdapter;
import de.edgesoft.refereemanager.model.RefereeAssignmentModel;
import de.edgesoft.refereemanager.model.RefereeQuantityModel;


/**
 * <p>Java class for EventDay complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EventDay"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{}AbstractModelClass"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="date" type="{}LocalDateProperty"/&gt;
 *         &lt;element name="start_time" type="{}LocalTimeProperty" minOccurs="0"/&gt;
 *         &lt;element name="end_time" type="{}LocalTimeProperty" minOccurs="0"/&gt;
 *         &lt;element name="referee_meeting_time" type="{}LocalTimeProperty" minOccurs="0"/&gt;
 *         &lt;element name="referee_quantity" type="{}RefereeQuantity" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="referee_assignment" type="{}RefereeAssignment" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EventDay", propOrder = {
    "date",
    "startTime",
    "endTime",
    "refereeMeetingTime",
    "refereeQuantity",
    "refereeAssignment"
})
public class EventDay
    extends AbstractModelClass
{

    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(SimpleObjectPropertyLocalDateAdapter.class)
    @XmlSchemaType(name = "date")
    protected javafx.beans.property.SimpleObjectProperty date;
    @XmlElement(name = "start_time", type = String.class)
    @XmlJavaTypeAdapter(SimpleObjectPropertyLocalTimeAdapter.class)
    @XmlSchemaType(name = "time")
    protected javafx.beans.property.SimpleObjectProperty startTime;
    @XmlElement(name = "end_time", type = String.class)
    @XmlJavaTypeAdapter(SimpleObjectPropertyLocalTimeAdapter.class)
    @XmlSchemaType(name = "time")
    protected javafx.beans.property.SimpleObjectProperty endTime;
    @XmlElement(name = "referee_meeting_time", type = String.class)
    @XmlJavaTypeAdapter(SimpleObjectPropertyLocalTimeAdapter.class)
    @XmlSchemaType(name = "time")
    protected javafx.beans.property.SimpleObjectProperty refereeMeetingTime;
    @XmlElement(name = "referee_quantity", type = RefereeQuantityModel.class)
    protected List<RefereeQuantity> refereeQuantity;
    @XmlElement(name = "referee_assignment", type = RefereeAssignmentModel.class)
    protected List<RefereeAssignment> refereeAssignment;

    /**
     * Gets the value of the date property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public javafx.beans.property.SimpleObjectProperty getDate() {
        return date;
    }

    /**
     * Sets the value of the date property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDate(javafx.beans.property.SimpleObjectProperty value) {
        this.date = value;
    }

    /**
     * Gets the value of the startTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public javafx.beans.property.SimpleObjectProperty getStartTime() {
        return startTime;
    }

    /**
     * Sets the value of the startTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartTime(javafx.beans.property.SimpleObjectProperty value) {
        this.startTime = value;
    }

    /**
     * Gets the value of the endTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public javafx.beans.property.SimpleObjectProperty getEndTime() {
        return endTime;
    }

    /**
     * Sets the value of the endTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndTime(javafx.beans.property.SimpleObjectProperty value) {
        this.endTime = value;
    }

    /**
     * Gets the value of the refereeMeetingTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public javafx.beans.property.SimpleObjectProperty getRefereeMeetingTime() {
        return refereeMeetingTime;
    }

    /**
     * Sets the value of the refereeMeetingTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefereeMeetingTime(javafx.beans.property.SimpleObjectProperty value) {
        this.refereeMeetingTime = value;
    }

    /**
     * Gets the value of the refereeQuantity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refereeQuantity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefereeQuantity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefereeQuantity }
     * 
     * 
     */
    public List<RefereeQuantity> getRefereeQuantity() {
        if (refereeQuantity == null) {
            refereeQuantity = new ArrayList<RefereeQuantity>();
        }
        return this.refereeQuantity;
    }

    /**
     * Gets the value of the refereeAssignment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refereeAssignment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefereeAssignment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefereeAssignment }
     * 
     * 
     */
    public List<RefereeAssignment> getRefereeAssignment() {
        if (refereeAssignment == null) {
            refereeAssignment = new ArrayList<RefereeAssignment>();
        }
        return this.refereeAssignment;
    }

}
