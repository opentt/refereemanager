
package de.edgesoft.refereemanager.jaxb;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import de.edgesoft.refereemanager.model.AddressModel;
import de.edgesoft.refereemanager.model.TitledIDTypeModel;


/**
 * 
 * 				Usually, a venue only has one address.
 * 				This proves difficult to use in my input form hierarchy controllers.
 * 				At the moment it is much easier to allow a list of addresses and use only one.
 * 			
 * 
 * <p>Java class for Venue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Venue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{}TitledIDType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="address" type="{}Address" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Venue", propOrder = {
    "address"
})
public class Venue
    extends TitledIDTypeModel
{

    @XmlElement(type = AddressModel.class)
    protected List<Address> address;

    /**
     * Gets the value of the address property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the address property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Address }
     * 
     * 
     */
    public List<Address> getAddress() {
        if (address == null) {
            address = new ArrayList<Address>();
        }
        return this.address;
    }

}
