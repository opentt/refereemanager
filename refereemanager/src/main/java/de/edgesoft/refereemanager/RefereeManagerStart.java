package de.edgesoft.refereemanager;

/**
 * RefereeManager application, class for avoiding jar issues with JavaFX components.
 *
 * See https://stackoverflow.com/questions/52653836/maven-shade-javafx-runtime-components-are-missing
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of Open-TT: Referee Manager.</p>
 *
 * <p>Open-TT: Referee Manager is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * <p>Open-TT: Referee Manager is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.16.0
 */
public class RefereeManagerStart {

	/**
	 * Starts the application.
	 *
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		RefereeManager.main(args);
	}

}

/* EOF */
