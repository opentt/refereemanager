package de.edgesoft.refereemanager.controller.details;

import de.edgesoft.edgeutils.files.Prefs;
import de.edgesoft.edgeutils.files.Resources;
import de.edgesoft.edgeutils.i18n.I18N;
import de.edgesoft.edgeutils.i18n.ResourceType;
import de.edgesoft.edgeutils.javafx.ControlUtils;
import de.edgesoft.edgeutils.javafx.LabeledUtils;
import de.edgesoft.edgeutils.javafx.controller.CopyTextPane;
import de.edgesoft.refereemanager.jaxb.RefereeAssignment;
import de.edgesoft.refereemanager.model.AppModel;
import de.edgesoft.refereemanager.model.ContentModel;
import de.edgesoft.refereemanager.model.EventDateModel;
import de.edgesoft.refereemanager.model.EventDayModel;
import de.edgesoft.refereemanager.model.RefereeAssignmentTypeModel;
import javafx.fxml.FXML;
import javafx.geometry.VPos;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;

/**
 * Controller for all event date details scenes.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.17.0
 */
public abstract class DetailsEventDateController<T extends EventDateModel> implements IDetailsController<T> {

	@FXML
	private Label lblHeading;
	@FXML
	private Label lblType;
	@FXML
	private Label lblVenues;
	@FXML
	private Label lblPeople;
	@FXML
	private Label lblTakesPlace;

	/**
	 * Type.
	 */
	@FXML
	private Label lblDisplayType;

	/**
	 * Days.
	 */
	@FXML
	private VBox boxDisplayDays;

	/**
	 * Venues.
	 */
	@FXML
	private VBox boxDisplayVenues;

	/**
	 * People.
	 */
	@FXML
	private VBox boxDisplayPeople;

	/**
	 * Takes place = not canceled.
	 */
	@FXML
	private Label lblDisplayTakesPlace;


	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	protected void initialize() {

		// icons
		ControlUtils.fillViewControls(ResourceType.TEXT, this,
				lblHeading,
				lblType,
				lblVenues,
				lblPeople,
				lblTakesPlace
				);

	}

	/**
	 * Shows detail data.
	 *
	 * @param theDetailData detail data (null if no data to show)
	 */
	@Override
	public void showDetails(final T theDetailData) {

		if (theDetailData == null) {

			LabeledUtils.setText(lblDisplayType, null);

			boxDisplayDays.getChildren().clear();
			boxDisplayVenues.getChildren().clear();
			boxDisplayPeople.getChildren().clear();

			LabeledUtils.setText(lblDisplayTakesPlace, null);

		} else {

			lblDisplayType.setText(
					(theDetailData.getType() == null) ?
							null :
							theDetailData.getType().getDisplayTitle().getValueSafe());

			boxDisplayDays.getChildren().clear();
			fillDaysBox(theDetailData);

			boxDisplayVenues.getChildren().clear();
			theDetailData.getVenue().stream().forEach(venue -> boxDisplayVenues.getChildren().add(CopyTextPane.createInstance(venue.getVenue().getDisplayTitleShort().getValueSafe())));

			boxDisplayPeople.getChildren().clear();
			theDetailData.getPerson().stream().forEach(person -> boxDisplayPeople.getChildren().add(CopyTextPane.createInstance(person.getPerson().getDisplayTitleShort().getValueSafe())));

			lblDisplayTakesPlace.setGraphic(new ImageView(Resources.loadImage(
					I18N.getText(String.format("refman.emblem.%s.icon", !theDetailData.isCanceled() ? "success" : "error")).get(),
					Prefs.SIZE_FLAG)));

		}

	}

	/**
	 * Fills days box with days data.
	 *
	 * @param theDetailData detail data (null if no data to show)
	 */
	private void fillDaysBox(
			final T theDetailData
			) {

		// pane
		GridPane pneDays = new GridPane();

		pneDays.setVgap(((GridPane) boxDisplayDays.getParent()).getVgap());
		((GridPane) boxDisplayDays.getParent()).getColumnConstraints().stream().forEach(constraint -> pneDays.getColumnConstraints().add(constraint));

		boxDisplayDays.getChildren().add(pneDays);

		theDetailData.getDay().stream().forEach(day -> {

			EventDayModel dayModel = (EventDayModel) day;

			// date
			Label lblDate = new Label(dayModel.getDateText().getValueSafe());
			lblDate.getStyleClass().add("refman-table-header-cell");

			// details
			VBox boxDayDetails = new VBox(5.0);

			if (dayModel.getTimeText() != null) {
				boxDayDetails.getChildren().add(new Label(dayModel.getTimeText().getValue()));
			}

			if (dayModel.getRefereeMeetingTimeText() != null) {
				boxDayDetails.getChildren().add(new Label(I18N.getText("view.details.detailstournament.refereemeetingtime.text", dayModel.getRefereeMeetingTimeText().getValue()).get()));
			}

			ContentModel.getSortedData(AppModel.getData().getContent().getRefereeAssignmentType(), RefereeAssignmentTypeModel.DEFAULT_COMPARATOR).stream().forEach(assignmenttype -> {

				int iNeeded = dayModel.getNeededRefereeCountFor(assignmenttype);
				int iAssigned = dayModel.getRefereeCountFor(assignmenttype);

				if ((iNeeded > 0) || (iAssigned > 0)) {

					CopyTextPane pneRefereeCount = CopyTextPane.createInstance(
							I18N.getText("view.details.detailstournament.refereetypestatistics.text",
								assignmenttype.getDisplayTitleShort().getValueSafe(),
								iNeeded,
								iAssigned).get(),
							false);
					AnchorPane.setRightAnchor(pneRefereeCount.getChildren().get(0), (Prefs.SIZE_FLAG * Resources.getScalingFactor()) * 2);
					Label lblRefereeCountIndicator = new Label();
					lblRefereeCountIndicator.setGraphic(new ImageView(Resources.loadImage(
							I18N.getText(String.format("refman.emblem.%s.icon", (iAssigned >= iNeeded) ? "success" : "error")).get(),
							Prefs.SIZE_FLAG)));
					pneRefereeCount.getChildren().add(lblRefereeCountIndicator);
					AnchorPane.setRightAnchor(lblRefereeCountIndicator, 0.0);
					boxDayDetails.getChildren().add(pneRefereeCount);

					int iRefCount = 0;
					for (RefereeAssignment theRefereeAssignment : dayModel.getRefereeAssignmentsFor(assignmenttype)) {
						boxDayDetails.getChildren().add(new Label(String.format("%5d. %s%s",
								++iRefCount,
								theRefereeAssignment.getReferee().getDisplayText().getValueSafe(),
								((theRefereeAssignment.getRemark() == null) || (theRefereeAssignment.getRemark().getValueSafe().isBlank())) ? "" : String.format(" (%s)", theRefereeAssignment.getRemark().getValue())
								)));
					}

				}

			});

			// add content
			pneDays.addRow(pneDays.getRowCount(), lblDate, boxDayDetails);

			RowConstraints rowConstraints = new RowConstraints();
			rowConstraints.setValignment(VPos.TOP);
			pneDays.getRowConstraints().add(rowConstraints);

		});

	}

}

/* EOF */
