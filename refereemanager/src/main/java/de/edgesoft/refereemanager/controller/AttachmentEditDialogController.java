package de.edgesoft.refereemanager.controller;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

import de.edgesoft.edgeutils.files.Prefs;
import de.edgesoft.edgeutils.files.Resources;
import de.edgesoft.refereemanager.messagetext.jaxb.Attachment;
import de.edgesoft.refereemanager.utils.LocalPrefs;
import de.edgesoft.refereemanager.utils.PrefKey;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * Controller for attachment edit dialog scene.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.10.0
 */
public class AttachmentEditDialogController {

	/**
	 * Filename text field.
	 */
	@FXML
	private TextField txtFilename;

	/**
	 * Filename button.
	 */
	@FXML
	private Button btnFilename;

	/**
	 * Title text field.
	 */
	@FXML
	private TextField txtTitle;

	/**
	 * Paper format portrait radio button.
	 */
	@FXML
	private RadioButton radPortrait;

	/**
	 * Paper format landscape radio button.
	 */
	@FXML
	private RadioButton radLandscape;

	/**
	 * OK button.
	 */
	@FXML
	private Button btnOK;

	/**
	 * Cancel button.
	 */
	@FXML
	private Button btnCancel;

	/**
	 * Reference to dialog stage.
	 *
	 * @since 0.10.0
	 */
	private Stage dialogStage;

	/**
	 * Current attachment.
	 */
	private Attachment currentAttachment;

	/**
	 * OK clicked?.
	 */
	private boolean okClicked;


	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {

		// enable ok button for valid entries only
		btnOK.disableProperty().bind(
				txtFilename.textProperty().isEmpty()
				);

		// icons
		btnOK.setGraphic(new ImageView(Resources.loadImage("icons/actions/dialog-ok.svg", Prefs.SIZE_BUTTON_SMALL)));
		btnCancel.setGraphic(new ImageView(Resources.loadImage("icons/actions/dialog-cancel.svg", Prefs.SIZE_BUTTON_SMALL)));
		btnFilename.setGraphic(new ImageView(Resources.loadImage("icons/actions/folder-open.svg", Prefs.SIZE_BUTTON_SMALL)));

	}

	/**
	 * Sets dialog stage.
	 *
	 * @param theStage dialog stage
	 */
	public void setDialogStage(final Stage theStage) {
		dialogStage = theStage;
	}

	/**
	 * Sets attachment to be edited.
	 *
	 * @param theAttachment edit attachment
	 */
	public void setAttachment(Attachment theAttachment) {

		Objects.requireNonNull(theAttachment);

		txtFilename.setText(theAttachment.getFilename().getValue());
		txtTitle.setText(theAttachment.getTitle().getValue());

		if (theAttachment.getLandscape().getValue()) {
			radLandscape.setSelected(true);
		} else {
			radPortrait.setSelected(true);
		}

		currentAttachment = theAttachment;

	}

	/**
	 * Returns if user clicked ok.
	 *
	 * @return did user click ok?
	 */
	public boolean isOkClicked() {
		return okClicked;
	}

	/**
	 * Validates input, stores ok click, and closes dialog; does nothing for invalid input.
	 */
	@FXML
	private void handleOk() {

		okClicked = false;

		currentAttachment.setFilename(new SimpleStringProperty(txtFilename.getText()));
		currentAttachment.setTitle(new SimpleStringProperty(txtTitle.getText()));
		currentAttachment.setLandscape(new SimpleBooleanProperty(radLandscape.isSelected()));

		okClicked = true;
		dialogStage.close();

	}

	/**
	 * Stores non-ok click and closes dialog.
	 */
	@FXML
	private void handleCancel() {
		okClicked = false;
		dialogStage.close();
	}

	/**
	 * Selects file, stores it to text field.
	 */
	@FXML
	private void handleFileSelect() {

		FileChooser fileChooser = new FileChooser();

		fileChooser.setTitle("Attachment auswählen");
		fileChooser.getExtensionFilters().addAll(
			new FileChooser.ExtensionFilter("Alle Dateien (*.*)", "*.*")
		);

		if (!LocalPrefs.get(PrefKey.REFEREE_COMMUNICATION_LAST_ATTACHMENT_PATH).isEmpty()) {
			Path pathFile = Paths.get(LocalPrefs.get(PrefKey.REFEREE_COMMUNICATION_LAST_ATTACHMENT_PATH));
			if (pathFile != null) {
				fileChooser.setInitialDirectory(pathFile.toFile());
			}
		}

		File file = fileChooser.showOpenDialog(dialogStage);

		if (file != null) {
			txtFilename.setText(file.getPath());
		}

	}

}

/* EOF */
