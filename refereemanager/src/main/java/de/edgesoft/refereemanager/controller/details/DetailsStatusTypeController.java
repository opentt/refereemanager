package de.edgesoft.refereemanager.controller.details;

import de.edgesoft.edgeutils.javafx.LabeledUtils;
import de.edgesoft.refereemanager.model.StatusTypeModel;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

/**
 * Controller for the status details scene.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public class DetailsStatusTypeController<T extends StatusTypeModel> implements IDetailsController<T> {

	/**
	 * Active?.
	 */
	@FXML
	private Label lblActive;


	/**
	 * Shows detail data.
	 *
	 * @param theDetailData (null if no data to show)
	 */
	@Override
	public void showDetails(final T theDetailData) {

		if (theDetailData == null) {

			LabeledUtils.setText(lblActive, null);

		} else {

			lblActive.setText(
					(theDetailData.getActive().getValue()) ?
							"aktiv" :
							"passiv");

		}

	}

}

/* EOF */
