package de.edgesoft.refereemanager.controller.datatables;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import de.edgesoft.edgeutils.i18n.I18N;
import de.edgesoft.edgeutils.javafx.FontUtils;
import de.edgesoft.refereemanager.documentgeneration.DataFeature;
import de.edgesoft.refereemanager.jaxb.Trainee;
import de.edgesoft.refereemanager.model.AppModel;
import de.edgesoft.refereemanager.model.ContentModel;
import de.edgesoft.refereemanager.model.PersonModel;
import de.edgesoft.refereemanager.model.TitledIDTypeModel;
import de.edgesoft.refereemanager.model.TraineeModel;
import de.edgesoft.refereemanager.utils.ExamResult;
import de.edgesoft.refereemanager.utils.TableUtils;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.geometry.Orientation;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.FontWeight;

/**
 * Controller for the trainee list scene.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.10.0
 */
public class DataTableTraineesController extends AbstractDataTableController<Trainee> {

	/**
	 * Container box.
	 */
	@FXML
	private VBox boxContainer;

	/**
	 * Table.
	 */
	@FXML
	private TableView<Trainee> tblData;

	/**
	 * ID column.
	 */
	@FXML
	private TableColumn<TraineeModel, String> colID;

	/**
	 * Name column.
	 */
	@FXML
	private TableColumn<TraineeModel, String> colName;

	/**
	 * First name column.
	 */
	@FXML
	private TableColumn<TraineeModel, String> colFirstName;

	/**
	 * Club column.
	 */
	@FXML
	private TableColumn<TraineeModel, String> colClub;

	/**
	 * Exam result column.
	 */
	@FXML
	private TableColumn<TraineeModel, String> colExamResult;

	/**
	 * Birthday column.
	 */
	@FXML
	private TableColumn<TraineeModel, LocalDate> colBirthday;


	/**
	 * Filter storage.
	 */
	private Map<CheckBox, ExamResult> mapTraineeFilterExamResults;


	/**
	 * List of trainees.
	 */
	private FilteredList<Trainee> lstTrainee;


	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	@Override
	protected void initialize() {

		super.initialize();

		// hook data to columns (referees)
		colID.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getId()));
		colID.setVisible(false);

		colName.setCellValueFactory(cellData -> cellData.getValue().getName());
		colFirstName.setCellValueFactory(cellData -> cellData.getValue().getFirstName());
		colClub.setCellValueFactory(cellData -> (cellData.getValue().getMember() == null) ? null : cellData.getValue().getMember().getDisplayText());

		colExamResult.setCellValueFactory(cellData -> new SimpleStringProperty(I18N.getText(cellData.getValue().getExamResult()).orElse(cellData.getValue().getExamResult().value())));

		colBirthday.setCellValueFactory(cellData -> cellData.getValue().getBirthday());
		colBirthday.setVisible(false);
		colBirthday.setCellFactory(column -> TableUtils.getTableCellTraineeDate(null));

		// headings
		lblStatus.setFont(FontUtils.getDerived(lblStatus.getFont(), FontWeight.BOLD));

		// setup exam result filter
		HBox boxExamResultFilter = new HBox(5);
		boxContainer.getChildren().add(new Separator(Orientation.HORIZONTAL));
		boxContainer.getChildren().add(boxExamResultFilter);

		mapTraineeFilterExamResults = new HashMap<>();
		EnumSet.allOf(ExamResult.class).forEach(
				examResult -> {
					CheckBox chkTemp = new CheckBox(I18N.getText(examResult).orElse(examResult.value()));
					chkTemp.setOnAction(e -> handleFilterChange());
					boxExamResultFilter.getChildren().add(chkTemp);
					mapTraineeFilterExamResults.put(chkTemp, examResult);
				}
		);

		// init items
		setDataTableItems();

	}

	/**
	 * Returns data table.
	 */
	@Override
	public TableView<Trainee> getDataTable() {
		return tblData;
	}

	/**
	 * Sets table items.
	 */
	@Override
	public void setDataTableItems() {

		lstTrainee = new FilteredList<>(((ContentModel) AppModel.getData().getContent()).getObservableTrainees(), trainee -> true);

		SortedList<Trainee> lstSortedRefs = new SortedList<>(lstTrainee);
		lstSortedRefs.comparatorProperty().bind(tblData.comparatorProperty());
		tblData.setItems(lstSortedRefs);

		setDataTablePlaceholderNoun("Azubi");

		handleFilterChange();

	}

	/**
	 * Handles filter change events.
	 */
	@FXML
	@Override
	public void handleFilterChange() {

		if (lstTrainee != null) {

			lstTrainee.setPredicate(TitledIDTypeModel.ALL);

			for (Entry<CheckBox, ExamResult> entryChkExamResult : mapTraineeFilterExamResults.entrySet()) {

				if (entryChkExamResult.getKey().isSelected()) {
					lstTrainee.setPredicate(((Predicate<Trainee>) lstTrainee.getPredicate()).and(TraineeModel.getExamResultPredicate(entryChkExamResult.getValue())));
				}

			}

		}

		updateTable();

	}

	/**
	 * Returns selection from table as sorted list.
	 *
	 * @return sorted selection from table
	 *
	 * @since 0.12.0
	 */
	@Override
	public ObservableList<Trainee> getSortedSelectedItems() {
		List<Trainee> lstReturn = new ArrayList<>();

		getSelectionModel().getSelectedItems().forEach(data -> lstReturn.add(data));

		return FXCollections.observableList(lstReturn.stream().sorted(PersonModel.NAME_FIRSTNAME).collect(Collectors.toList()));
	}

	@Override
	public Optional<Set<DataFeature>> getProvidedDataFeatures() {
		return Optional.of(Set.of(DataFeature.HAS_ADDRESS, DataFeature.HAS_EMAIL));
	}

}

/* EOF */
