package de.edgesoft.refereemanager.controller.details;

import de.edgesoft.edgeutils.javafx.controller.AbstractHyperlinkController;
import de.edgesoft.edgeutils.javafx.controller.CopyTextPane;
import de.edgesoft.refereemanager.model.AddressModel;
import de.edgesoft.refereemanager.model.VenueModel;
import javafx.fxml.FXML;
import javafx.scene.layout.VBox;

/**
 * Controller for the venue details scene.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public class DetailsVenueController<T extends VenueModel> extends AbstractHyperlinkController implements IDetailsController<T> {

	/**
	 * Address box.
	 */
	@FXML
	private VBox boxAddress;

	/**
	 * Shows detail data.
	 *
	 * @param theDetailData (null if no data to show)
	 */
	@Override
	public void showDetails(final T theDetailData) {

		if (theDetailData == null) {

			boxAddress.getChildren().clear();

		} else {

			boxAddress.getChildren().clear();
			theDetailData.getAddress().stream().forEach(address -> boxAddress.getChildren().add(CopyTextPane.createInstance(((AddressModel) address).getDisplayText(true).getValueSafe(), true)));

		}

	}

}

/* EOF */
