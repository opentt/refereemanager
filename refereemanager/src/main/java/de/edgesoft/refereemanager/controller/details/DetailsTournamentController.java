package de.edgesoft.refereemanager.controller.details;

import de.edgesoft.edgeutils.files.Prefs;
import de.edgesoft.edgeutils.files.Resources;
import de.edgesoft.edgeutils.i18n.I18N;
import de.edgesoft.edgeutils.i18n.ResourceType;
import de.edgesoft.edgeutils.javafx.ControlUtils;
import de.edgesoft.edgeutils.javafx.LabeledUtils;
import de.edgesoft.edgeutils.javafx.controller.CopyTextPane;
import de.edgesoft.refereemanager.model.TournamentModel;
import de.edgesoft.refereemanager.model.URLModel;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

/**
 * Controller for the tournament details scene.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public class DetailsTournamentController<T extends TournamentModel> extends DetailsEventDateController<T> {

	@FXML
	private Label lblRefereeReport;
	@FXML
	private Label lblAnnouncements;
	@FXML
	private Label lblInformation;
	@FXML
	private Label lblResults;
	@FXML
	private Label lblClub;

	/**
	 * Club label.
	 */
	@FXML
	private Label lblDisplayClub;

	/**
	 * Referee report.
	 */
	@FXML
	private VBox boxDisplayRefereeReport;

	/**
	 * Announcements.
	 */
	@FXML
	private VBox boxDisplayAnnouncements;

	/**
	 * Information.
	 */
	@FXML
	private VBox boxDisplayInformation;

	/**
	 * Results.
	 */
	@FXML
	private VBox boxDisplayResults;


	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	@Override
	protected void initialize() {

		super.initialize();

		// icons
		ControlUtils.fillViewControls(ResourceType.TEXT, this,
				lblRefereeReport,
				lblAnnouncements,
				lblInformation,
				lblResults,
				lblClub
				);

	}

	/**
	 * Shows detail data.
	 *
	 * @param theDetailData detail data (null if no data to show)
	 */
	@Override
	public void showDetails(final T theDetailData) {

		super.showDetails(theDetailData);

		if (theDetailData == null) {

			LabeledUtils.setText(lblDisplayClub, null);

			boxDisplayRefereeReport.getChildren().clear();
			boxDisplayAnnouncements.getChildren().clear();
			boxDisplayInformation.getChildren().clear();
			boxDisplayResults.getChildren().clear();

		} else {

			lblDisplayClub.setText(
					(theDetailData.getOrganizingClub() == null) ?
							null :
							theDetailData.getOrganizingClub().getDisplayTitleShort().getValueSafe());

			boxDisplayRefereeReport.getChildren().clear();
			CopyTextPane pneRefereeReport = CopyTextPane.createInstance(theDetailData.getRefereeReportFilename().getValueSafe(), true);
			if (theDetailData.existsRefereeReportFile() != null) {
				AnchorPane.setRightAnchor(pneRefereeReport.getChildren().get(0), (Prefs.SIZE_FLAG * Resources.getScalingFactor()) * 4);
				Labeled lblRefereeReportIndicator = new Label();
				lblRefereeReportIndicator.setGraphic(new ImageView(Resources.loadImage(
						I18N.getText(String.format("refman.emblem.%s.icon", theDetailData.existsRefereeReportFile().get() ? "success" : "error")).get(),
						Prefs.SIZE_FLAG)));
				pneRefereeReport.getChildren().add(lblRefereeReportIndicator);
				AnchorPane.setRightAnchor(lblRefereeReportIndicator, (Resources.getDefaultWidth() * Resources.getScalingFactor()) * 2);
			}
			boxDisplayRefereeReport.getChildren().add(pneRefereeReport);

			boxDisplayAnnouncements.getChildren().clear();
			theDetailData.getAnnouncementURL().stream().forEach(announcement ->
					boxDisplayAnnouncements.getChildren().add(CopyTextPane.createInstance(((URLModel) announcement).getRemarkedText().getValueSafe(), true))
			);

			boxDisplayInformation.getChildren().clear();
			theDetailData.getInformationURL().stream().forEach(information ->
					boxDisplayInformation.getChildren().add(CopyTextPane.createInstance(((URLModel) information).getRemarkedText().getValueSafe(), true))
			);

			boxDisplayResults.getChildren().clear();
			theDetailData.getResultURL().stream().forEach(result ->
					boxDisplayResults.getChildren().add(CopyTextPane.createInstance(((URLModel) result).getRemarkedText().getValueSafe(), true))
			);

		}

	}

}

/* EOF */
