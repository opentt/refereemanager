package de.edgesoft.refereemanager.controller.editdialogs;
import java.util.ArrayList;
import java.util.Arrays;

import de.edgesoft.edgeutils.commons.IDType;
import de.edgesoft.edgeutils.i18n.ResourceType;
import de.edgesoft.edgeutils.javafx.TabUtils;
import de.edgesoft.refereemanager.controller.inputforms.IInputFormController;
import de.edgesoft.refereemanager.jaxb.EventDate;
import de.edgesoft.refereemanager.jaxb.EventDay;
import de.edgesoft.refereemanager.jaxb.PersonVenueReferrer;
import de.edgesoft.refereemanager.jaxb.TitledIDType;
import de.edgesoft.refereemanager.jaxb.Tournament;
import de.edgesoft.refereemanager.jaxb.URL;
import javafx.fxml.FXML;
import javafx.scene.control.Tab;

/**
 * Controller for the tournament edit dialog scene.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public class EditDialogTournamentController extends AbstractTabbedEditDialogController<Tournament> {

	/**
	 * Titled id controller.
	 */
	@FXML
	private IInputFormController<Tournament> embeddedInputFormTitledIDTypeController;

	/**
	 * Event data controller.
	 */
	@FXML
	private IInputFormController<Tournament> embeddedInputFormEventDateController;

	/**
	 * Tournament controller.
	 */
	@FXML
	private IInputFormController<Tournament> embeddedInputFormTournamentController;

	/**
	 * Event day controller.
	 */
	@FXML
	private IInputFormController<Tournament> embeddedInputFormEventDayController;

	/**
	 * Announcement controller.
	 */
	@FXML
	private IInputFormController<Tournament> embeddedInputFormAnnouncementController;

	/**
	 * Information controller.
	 */
	@FXML
	private IInputFormController<Tournament> embeddedInputFormInformationController;

	/**
	 * Result controller.
	 */
	@FXML
	private IInputFormController<Tournament> embeddedInputFormResultController;

	/**
	 * Venues controller.
	 */
	@FXML
	private IInputFormController<Tournament> embeddedInputFormVenueReferenceController;

	/**
	 * Contact people controller.
	 */
	@FXML
	private IInputFormController<Tournament> embeddedInputFormPersonReferenceController;

	@FXML
	private Tab tabTitledID;
	@FXML
	private Tab tabEvent;
	@FXML
	private Tab tabTournament;
	@FXML
	private Tab tabDays;
	@FXML
	private Tab tabAnnouncements;
	@FXML
	private Tab tabInformation;
	@FXML
	private Tab tabResults;
	@FXML
	private Tab tabVenues;
	@FXML
	private Tab tabOrganizers;

	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	@Override
	protected void initialize() {

		addInputFormController(embeddedInputFormTitledIDTypeController);
		addInputFormController(embeddedInputFormEventDateController);
		addInputFormController(embeddedInputFormTournamentController);
		addInputFormController(embeddedInputFormEventDayController);
		addInputFormController(embeddedInputFormAnnouncementController);
		addInputFormController(embeddedInputFormInformationController);
		addInputFormController(embeddedInputFormResultController);
		addInputFormController(embeddedInputFormVenueReferenceController);
		addInputFormController(embeddedInputFormPersonReferenceController);

		initForm(new ArrayList<>(Arrays.asList(new Class<?>[]{
				IDType.class, TitledIDType.class,
				EventDate.class, EventDay.class, PersonVenueReferrer.class,
				Tournament.class, URL.class
		})));

		super.initialize();

		TabUtils.fillTabs(this, ResourceType.TEXT,
				tabTitledID,
				tabEvent,
				tabTournament,
				tabDays,
				tabAnnouncements,
				tabInformation,
				tabResults,
				tabVenues,
				tabOrganizers
				);

	}

}

/* EOF */
