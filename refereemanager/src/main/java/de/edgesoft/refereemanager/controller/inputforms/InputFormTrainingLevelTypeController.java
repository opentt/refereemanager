package de.edgesoft.refereemanager.controller.inputforms;
import de.edgesoft.refereemanager.jaxb.TrainingLevelType;
import de.edgesoft.refereemanager.utils.JAXBMatch;
import de.edgesoft.refereemanager.utils.SpinnerUtils;
import javafx.fxml.FXML;
import javafx.scene.control.Spinner;

/**
 * Controller for the training level type data edit dialog tab.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public class InputFormTrainingLevelTypeController extends AbstractInputFormController<TrainingLevelType> {

	/**
	 * Spinner for rank.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "rank", jaxbclass = TrainingLevelType.class)
	protected Spinner<Integer> spnRank;

	/**
	 * Spinner for update interval.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "updateInterval", jaxbclass = TrainingLevelType.class)
	protected Spinner<Integer> spnUpdateInterval;


	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	protected void initialize() {

        // setup spinners
        SpinnerUtils.prepareIntegerSpinner(spnRank, 0, 100);
        SpinnerUtils.prepareIntegerSpinner(spnUpdateInterval, 0, 10);

	}

}

/* EOF */
