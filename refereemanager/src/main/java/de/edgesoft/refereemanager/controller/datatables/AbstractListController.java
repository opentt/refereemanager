package de.edgesoft.refereemanager.controller.datatables;

import java.util.Optional;

import de.edgesoft.refereemanager.model.TitledIDTypeModel;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.scene.control.SelectionMode;

/**
 * Abstract controller for list scenes.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public abstract class AbstractListController<T extends TitledIDTypeModel> implements IListController<T> {

	/**
	 * Sets selected item.
	 *
	 * @param theItem item to select
	 */
	@Override
	public void select(final T theItem) {
		getSelectionModel().select(theItem);
	}

	/**
	 * Sets selection mode.
	 *
	 * @param theSelectionMode selection mode
	 */
	@Override
	public void setSelectionMode(final SelectionMode theSelectionMode) {
		getSelectionModel().setSelectionMode(theSelectionMode);
	}

	/**
	 * Returns selected item property.
	 *
	 * @return selected item property
	 */
	@Override
	public ReadOnlyObjectProperty<T> selectedItemProperty() {
		return getSelectionModel().selectedItemProperty();
	}

	/**
	 * Returns selected item if there is only one.
	 *
	 * @return selected item
	 */
	@Override
	public Optional<T> getSelectedItem() {

		if (getSelectionModel().getSelectedItems().size() != 1) {
			return Optional.empty();
		}

		return Optional.of(getSelectionModel().getSelectedItem());

	}

}

/* EOF */
