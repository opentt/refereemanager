package de.edgesoft.refereemanager.controller.datatables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import de.edgesoft.edgeutils.i18n.ResourceType;
import de.edgesoft.edgeutils.javafx.TableColumnUtils;
import de.edgesoft.refereemanager.jaxb.EventDateType;
import de.edgesoft.refereemanager.jaxb.OtherEvent;
import de.edgesoft.refereemanager.model.AppModel;
import de.edgesoft.refereemanager.model.ContentModel;
import de.edgesoft.refereemanager.model.EventDateModel;
import de.edgesoft.refereemanager.model.TitledIDTypeModel;
import de.edgesoft.refereemanager.utils.BooleanFlagCell;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.geometry.Orientation;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Separator;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * Controller for the other event list scene.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public class DataTableOtherEventsController extends AbstractDataTableController<OtherEvent> {

	/**
	 * Container.
	 */
	@FXML
	private VBox boxContainer;

	/**
	 * Table other events.
	 */
	@FXML
	private TableView<OtherEvent> tblData;

	/**
	 * ID column.
	 */
	@FXML
	private TableColumn<EventDateModel, String> colID;

	/**
	 * Title column.
	 */
	@FXML
	private TableColumn<EventDateModel, String> colTitle;

	/**
	 * Date column.
	 */
	@FXML
	private TableColumn<EventDateModel, String> colDate;

	/**
	 * Type column.
	 */
	@FXML
	private TableColumn<EventDateModel, String> colType;

	/**
	 * Takes place column.
	 */
	@FXML
	private TableColumn<EventDateModel, Boolean> colTakesPlace;


	/**
	 * Filter storage.
	 */
	private Map<CheckBox, EventDateType> mapEventFilterTypes;


	/**
	 * List of other events.
	 */
	private FilteredList<OtherEvent> lstOtherEvents;


	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	@Override
	protected void initialize() {

		super.initialize();

		TableColumnUtils.fillTableColumns(this, ResourceType.TEXT,
				colID,
				colTitle,
				colDate,
				colType,
				colTakesPlace
				);

		// hook data to columns
		colID.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getId()));
		colID.setVisible(false);

		colTitle.setCellValueFactory(cellData -> cellData.getValue().getDisplayTitleShort());
		colDate.setCellValueFactory(cellData -> cellData.getValue().getDateRangeText());
		colType.setCellValueFactory(cellData -> cellData.getValue().getType().getDisplayTitle());

		colTakesPlace.setCellValueFactory(cellData -> cellData.getValue().getCanceled().not());
		colTakesPlace.setCellFactory(new Callback<TableColumn<EventDateModel,Boolean>, TableCell<EventDateModel,Boolean>>() {

			@Override
			public TableCell<EventDateModel, Boolean> call(TableColumn<EventDateModel, Boolean> param) {
				return new BooleanFlagCell<>();
			}

        });

		// setup type filter
		HBox boxTypeFilter = new HBox(5);
		boxContainer.getChildren().add(new Separator(Orientation.HORIZONTAL));
		boxContainer.getChildren().add(boxTypeFilter);

		mapEventFilterTypes = new HashMap<>();
		AppModel.getData().getContent().getEventDateType().stream().sorted(TitledIDTypeModel.SHORTTITLE_TITLE).forEach(
				eventDateType -> {
					CheckBox chkTemp = new CheckBox(eventDateType.getDisplayTitleShort().getValueSafe());
					chkTemp.setOnAction(e -> handleFilterChange());
					boxTypeFilter.getChildren().add(chkTemp);
					mapEventFilterTypes.put(chkTemp, eventDateType);
				}
		);

		// init items
		setDataTableItems();

	}

	/**
	 * Returns data table.
	 */
	@Override
	public TableView<OtherEvent> getDataTable() {
		return tblData;
	}

	/**
	 * Sets table items.
	 */
	@Override
	public void setDataTableItems() {

		lstOtherEvents = new FilteredList<>(((ContentModel) AppModel.getData().getContent()).getObservableOtherEvents(), otherevent -> true);

		SortedList<OtherEvent> lstSortedOtherEvents = new SortedList<>(lstOtherEvents);
		lstSortedOtherEvents.comparatorProperty().bind(tblData.comparatorProperty());
		tblData.setItems(lstSortedOtherEvents);

		setDataTablePlaceholderNoun("sonstigen Termine");

		handleFilterChange();

	}

	/**
	 * Handles filter change events.
	 */
	@SuppressWarnings("unchecked")
	@FXML
	@Override
	public void handleFilterChange() {

		// filter for events
		if (lstOtherEvents != null) {

			lstOtherEvents.setPredicate(TitledIDTypeModel.ALL);

			for (Entry<CheckBox, EventDateType> entryChkType : mapEventFilterTypes.entrySet()) {

				if (entryChkType.getKey().isSelected()) {
					lstOtherEvents.setPredicate(((Predicate<OtherEvent>) lstOtherEvents.getPredicate()).and(EventDateModel.getTypePredicate(entryChkType.getValue())));
				}

			}

		}

		updateTable();

	}

	/**
	 * Returns selection from table as sorted list.
	 *
	 * @return sorted selection from table
	 */
	@Override
	public ObservableList<OtherEvent> getSortedSelectedItems() {
		List<OtherEvent> lstReturn = new ArrayList<>();

		getSelectionModel().getSelectedItems().forEach(data -> lstReturn.add(data));

		return FXCollections.observableList(lstReturn.stream().sorted(EventDateModel.DATE_FIRST).collect(Collectors.toList()));
	}

}

/* EOF */
