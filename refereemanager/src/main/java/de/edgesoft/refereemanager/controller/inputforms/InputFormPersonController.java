package de.edgesoft.refereemanager.controller.inputforms;
import de.edgesoft.edgeutils.commons.IDType;
import de.edgesoft.edgeutils.commons.ext.ModelClassExt;
import de.edgesoft.edgeutils.datetime.DateTimeUtils;
import de.edgesoft.edgeutils.files.Prefs;
import de.edgesoft.edgeutils.files.Resources;
import de.edgesoft.edgeutils.javafx.ButtonUtils;
import de.edgesoft.edgeutils.javafx.EdgeDatePicker;
import de.edgesoft.refereemanager.jaxb.Person;
import de.edgesoft.refereemanager.jaxb.TitledIDType;
import de.edgesoft.refereemanager.model.AppModel;
import de.edgesoft.refereemanager.utils.ComboBoxUtils;
import de.edgesoft.refereemanager.utils.JAXBMatch;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

/**
 * Controller for the person data edit dialog tab.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public class InputFormPersonController extends AbstractInputFormController<Person> {

	/**
	 * ID text field.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "id", jaxbclass = IDType.class)
	protected TextField txtID;

	/**
	 * Title text field.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "title", jaxbclass = TitledIDType.class)
	protected TextField txtTitle;

	/**
	 * First name text field.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "firstName", jaxbclass = Person.class)
	protected TextField txtFirstName;

	/**
	 * Name text field label.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "name", jaxbclass = Person.class)
	protected Label lblName;

	/**
	 * Name text field.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "name", jaxbclass = Person.class)
	protected TextField txtName;

	/**
	 * Birthday picker.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "birthday", jaxbclass = Person.class)
	protected EdgeDatePicker pckBirthday;

	/**
	 * Day of death picker.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "dayOfDeath", jaxbclass = Person.class)
	protected EdgeDatePicker pckDayOfDeath;

	/**
	 * Combobox for sex types.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "sexType", jaxbclass = Person.class)
	protected ComboBox<ModelClassExt> cboSexType;

	/**
	 * Clear sex types.
	 */
	@FXML
	private Button btnSexTypeClear;

	/**
	 * Combobox for roles.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "role", jaxbclass = Person.class)
	protected ComboBox<ModelClassExt> cboRole;

	/**
	 * Clear roles.
	 */
	@FXML
	private Button btnRoleClear;

	/**
	 * Text area for remark.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "remark", jaxbclass = TitledIDType.class)
	protected TextArea txtRemark;


	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	protected void initialize() {

		// set date picker date format
		pckBirthday.setConverter(DateTimeUtils.getDateConverter("d.M.yyyy"));
        pckDayOfDeath.setConverter(DateTimeUtils.getDateConverter("d.M.yyyy"));

		// fill combo boxes
        ComboBoxUtils.prepareComboBox(cboSexType, AppModel.getData().getContent().getSexType());
        ComboBoxUtils.prepareComboBox(cboRole, AppModel.getData().getContent().getRoleType());

		// enable buttons
        ButtonUtils.bindDisable(btnSexTypeClear, cboSexType);
        ButtonUtils.bindDisable(btnRoleClear, cboRole);

		// icons
		btnSexTypeClear.setGraphic(new ImageView(Resources.loadImage("icons/actions/edit-clear.svg", Prefs.SIZE_BUTTON_SMALL)));
		btnRoleClear.setGraphic(new ImageView(Resources.loadImage("icons/actions/edit-clear.svg", Prefs.SIZE_BUTTON_SMALL)));

	}

	/**
	 * Clears sex type selection.
	 */
	@FXML
	private void handleSexTypeClear() {
		de.edgesoft.edgeutils.javafx.ComboBoxUtils.clearSelection(cboSexType);
	}

	/**
	 * Clears role selection.
	 */
	@FXML
	private void handleRoleClear() {
		de.edgesoft.edgeutils.javafx.ComboBoxUtils.clearSelection(cboRole);
	}

}

/* EOF */
