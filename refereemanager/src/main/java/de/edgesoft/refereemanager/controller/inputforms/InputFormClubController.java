package de.edgesoft.refereemanager.controller.inputforms;
import de.edgesoft.edgeutils.javafx.FontUtils;
import de.edgesoft.refereemanager.jaxb.Club;
import de.edgesoft.refereemanager.utils.JAXBMatch;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.FontPosture;

/**
 * Controller for the club data edit dialog tab.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public class InputFormClubController extends AbstractInputFormController<Club> {

	/**
	 * Checkbox for local clubs.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "isLocal", jaxbclass = Club.class)
	protected CheckBox chkIsLocal;

	/**
	 * Filename.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "filename", jaxbclass = Club.class)
	protected TextField txtFilename;

	/**
	 * Generated filename label.
	 */
	@FXML
	protected Label lblGeneratedFilename;

	/**
	 * Homepage.
	 */
	@FXML
	@JAXBMatch(jaxbfield = "homepage", jaxbclass = Club.class)
	protected TextField txtHomepage;


	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	protected void initialize() {

		lblGeneratedFilename.setFont(FontUtils.getDerived(lblGeneratedFilename.getFont(), FontPosture.ITALIC, -1));

//		txtTitle.textProperty().addListener((obs, oldText, newText) -> showGeneratedFilename());
//		txtShorttitle.textProperty().addListener((obs, oldText, newText) -> showGeneratedFilename());
		showGeneratedFilename();

	}

	/**
	 * Shows generated filename as remark.
	 */
	private void showGeneratedFilename() {

		lblGeneratedFilename.setText("todo");

//		String sGeneratedFilename = "";
//
//		// check for null: catch cases in which GUI is not initialized properly
//		if (txtTitle.getText() != null) {
//			sGeneratedFilename = txtTitle.getText();
//		}
//
//		if (txtShorttitle.getText() != null) {
//			if (!txtShorttitle.getText().isEmpty()) {
//				sGeneratedFilename = txtShorttitle.getText();
//			}
//		}
//
//		if (lblGeneratedFilename != null) {
//			lblGeneratedFilename.setText(MessageFormat.format("Generierter Dateiname: {0}", FileUtils.cleanFilename(sGeneratedFilename, false)));
//		}

	}

}

/* EOF */
