package de.edgesoft.refereemanager.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.Optional;
import java.util.prefs.BackingStoreException;
import java.util.prefs.InvalidPreferencesFormatException;

import de.edgesoft.edgeutils.files.Prefs;
import de.edgesoft.edgeutils.files.Resources;
import de.edgesoft.edgeutils.i18n.ResourceType;
import de.edgesoft.edgeutils.javafx.ControlUtils;
import de.edgesoft.edgeutils.javafx.TabUtils;
import de.edgesoft.refereemanager.utils.AlertUtils;
import de.edgesoft.refereemanager.utils.LocalPrefs;
import de.edgesoft.refereemanager.utils.PrefKey;
import de.edgesoft.refereemanager.utils.SpinnerUtils;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Spinner;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * Controller for preferences edit dialog scene.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.10.0
 */
public class PreferencesDialogController {

	/**
	 * OK button.
	 */
	@FXML
	private Button btnOK;

	/**
	 * Cancel button.
	 */
	@FXML
	private Button btnCancel;

	/**
	 * Import button.
	 */
	@FXML
	private Button btnImport;

	/**
	 * Export button.
	 */
	@FXML
	private Button btnExport;


	/**
	 * Tab pane.
	 */
	@FXML
	private TabPane pneTabs;


	/**
	 * Tab other.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private Tab tabOther;

	/**
	 * Checkbox: full path in title.
	 */
	@FXML
	private CheckBox chkTitleFullpath;

	/**
	 * Checkbox: sort data after loading.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private CheckBox chkDataSortLoading;

	/**
	 * Button size.
	 *
	 * @since 0.16.0
	 */
	@FXML
	private Slider sldButtonSize;

	/**
	 * Decimal places for geographic locations.
	 *
	 * @since 0.17.0
	 */
	@FXML
	protected Spinner<Integer> spnGeoDecimalPlaces;

	@FXML
	private Label lblTitleFullpathLabel;
	@FXML
	private Label lblDataSortLoadingLabel;
	@FXML
	private Label lblButtonSize;
	@FXML
	private Label lblGeoDecimalPlaces;

	/**
	 * Tab paths.
	 */
	@FXML
	private Tab tabPaths;

	/**
	 * Template path.
	 */
	@FXML
	private TextField txtPathsTemplate;

	/**
	 * Template path button.
	 */
	@FXML
	private Button btnPathsTemplate;

	/**
	 * Image path.
	 */
	@FXML
	private TextField txtPathsImage;

	/**
	 * Image path button.
	 */
	@FXML
	private Button btnPathsImage;

	/**
	 * Output path.
	 */
	@FXML
	private TextField txtPathsOutput;

	/**
	 * Output path button.
	 */
	@FXML
	private Button btnPathsOutput;

	/**
	 * Schema path.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private TextField txtPathsXSD;

	/**
	 * Schema path button.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private Button btnPathsXSD;

	@FXML
	private Label lblPathsPaths;
	@FXML
	private Label lblPathsTemplate;
	@FXML
	private Label lblPathsImage;
	@FXML
	private Label lblPathsOutput;
	@FXML
	private Label lblPathsOutputRemark1;
	@FXML
	private Label lblPathsSchema;
	@FXML
	private Label lblPathsXSD;


	/**
	 * Tab referee report.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private Tab tabRefereeReports;

	/**
	 * Report path.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private TextField txtRefereeReportPath;

	/**
	 * Report path button.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private Button btnRefereeReportPath;

	/**
	 * Report filename league games.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private TextField txtRefereeReportLeagueGame;

	/**
	 * Report filename tournaments.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private TextField txtRefereeReportTournament;

	@FXML
	private Label lblRefereeReportPath;
	@FXML
	private Label lblRefereeReportPathRemark1;
	@FXML
	private Label lblRefereeReportLeagueGame;
	@FXML
	private Label lblRefereeReportLeagueGameRemark1;
	@FXML
	private Label lblRefereeReportLeagueGameRemark2;
	@FXML
	private Label lblRefereeReportLeagueGameRemark3;
	@FXML
	private Label lblRefereeReportLeagueGameRemark4;
	@FXML
	private Label lblRefereeReportTournament;
	@FXML
	private Label lblRefereeReportTournamentRemark1;
	@FXML
	private Label lblRefereeReportTournamentRemark2;


	/**
	 * Tab EMail.
	 */
	@FXML
	private Tab tabEMail;

	/**
	 * EMail - SMTP host.
	 */
	@FXML
	private TextField txtEMailSMTPHost;

	/**
	 * EMail - SMTP username.
	 */
	@FXML
	private TextField txtEMailSMTPUsername;

	/**
	 * EMail - SMTP password.
	 */
	@FXML
	private TextField txtEMailSMTPPassword;

	/**
	 * EMail - From name.
	 */
	@FXML
	private TextField txtEMailFromName;

	/**
	 * EMail - From ameil.
	 */
	@FXML
	private TextField txtEMailFromEMail;

	/**
	 * EMail - Reply To name 1.
	 */
	@FXML
	private TextField txtEMailReplyToName1;

	/**
	 * EMail - Reply To email 1.
	 */
	@FXML
	private TextField txtEMailReplyToEMail1;

	/**
	 * EMail - Reply To name 2.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private TextField txtEMailReplyToName2;

	/**
	 * EMail - Reply To email 2.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private TextField txtEMailReplyToEMail2;

	/**
	 * EMail - Templates - EMail.
	 */
	@FXML
	private TextField txtEMailTemplateEMail;

	@FXML
	private Label lblEMailSMTP;
	@FXML
	private Label lblEMailSMTPHost;
	@FXML
	private Label lblEMailSMTPUsername;
	@FXML
	private Label lblEMailSMTPPassword;
	@FXML
	private Label lblEMailSMTPPasswordRemark1;
	@FXML
	private Label lblEMailFrom;
	@FXML
	private Label lblEMailFromName;
	@FXML
	private Label lblEMailFromEMail;
	@FXML
	private Label lblEMailTemplate;
	@FXML
	private Label lblEMailTemplateEMail;
	@FXML
	private Label lblEMailPause;
	@FXML
	private Label lblEMailPauseDuration;
	@FXML
	private Spinner<Integer> spnEMailPauseDuration;
	@FXML
	private Label lblEMailPauseFrequency;
	@FXML
	private Spinner<Integer> spnEMailPauseFrequency;
	@FXML
	private Label lblEMailReplyTo;
	@FXML
	private Label lblEMailReplyToName1;
	@FXML
	private Label lblEMailReplyToEMail1;
	@FXML
	private Label lblEMailReplyToName2;
	@FXML
	private Label lblEMailReplyToEMail2;


	/**
	 * Tab letters.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private Tab tabLetters;

	/**
	 * Letters - Templates - Letter.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private TextField txtLettersTemplateLetter;

	/**
	 * Letters - Templates - single merge.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private TextField txtLettersTemplateMergeSingle;

	/**
	 * Letters - Templates - all merge.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private TextField txtLettersTemplateMergeAll;

	@FXML
	private Label lblLettersTemplate;
	@FXML
	private Label lblLettersTemplateLetter;
	@FXML
	private Label lblLettersTemplateMergeSingle;
	@FXML
	private Label lblLettersTemplateMergeAll;


	/**
	 * Tab documents.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private Tab tabDocuments;

	/**
	 * Documents - Templates - Document.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private TextField txtDocumentsTemplateDocument;

	@FXML
	private Label lblDocumentsTemplate;
	@FXML
	private Label lblDocumentsTemplateDocument;


	/**
	 * Tab texts.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private Tab tabTexts;

	/**
	 * Texts - Templates - Text.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private TextField txtTextsTemplateText;

	@FXML
	private Label lblTextsTemplate;
	@FXML
	private Label lblTextsTemplateText;


	/**
	 * Tab statistics.
	 *
	 * @since 0.14.0
	 */
	@FXML
	private Tab tabStatistics;

	/**
	 * Statistics - Templates - Overview.
	 *
	 * @since 0.14.0
	 */
	@FXML
	private TextField txtStatisticsTemplateOverview;

	@FXML
	private Label lblStatisticsTemplate;
	@FXML
	private Label lblStatisticsTemplateOverview;


	/**
	 * Reference to dialog stage.
	 */
	private Stage dialogStage;

	/**
	 * OK clicked?.
	 */
	private boolean okClicked;


	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {

		// icons
		ControlUtils.fillViewControl(btnOK, this, Prefs.SIZE_BUTTON_SMALL, ResourceType.TEXT, ResourceType.ICON);
		ControlUtils.fillViewControl(btnCancel, this, Prefs.SIZE_BUTTON_SMALL, ResourceType.TEXT, ResourceType.ICON);

		ControlUtils.fillViewControls(ResourceType.TEXT, this,

				lblTitleFullpathLabel,
				chkTitleFullpath,
				lblDataSortLoadingLabel,
				chkDataSortLoading,
				lblButtonSize,
				lblGeoDecimalPlaces,

				lblPathsPaths,
				lblPathsTemplate,
				lblPathsImage,
				lblPathsOutput,
				lblPathsOutputRemark1,
				lblPathsSchema,
				lblPathsXSD,

				lblRefereeReportPath,
				lblRefereeReportPathRemark1,
				lblRefereeReportLeagueGame,
				lblRefereeReportLeagueGameRemark1,
				lblRefereeReportLeagueGameRemark2,
				lblRefereeReportLeagueGameRemark3,
				lblRefereeReportLeagueGameRemark4,
				lblRefereeReportTournament,
				lblRefereeReportTournamentRemark1,
				lblRefereeReportTournamentRemark2,

				lblEMailSMTP,
				lblEMailSMTPHost,
				lblEMailSMTPUsername,
				lblEMailSMTPPassword,
				lblEMailSMTPPasswordRemark1,
				lblEMailFrom,
				lblEMailFromName,
				lblEMailFromEMail,
				lblEMailTemplate,
				lblEMailTemplateEMail,
				lblEMailPause,
				lblEMailPauseDuration,
				lblEMailPauseFrequency,
				lblEMailReplyTo,
				lblEMailReplyToName1,
				lblEMailReplyToEMail1,
				lblEMailReplyToName2,
				lblEMailReplyToEMail2,

				lblLettersTemplate,
				lblLettersTemplateLetter,
				lblLettersTemplateMergeSingle,
				lblLettersTemplateMergeAll,

				lblDocumentsTemplate,
				lblDocumentsTemplateDocument,

				lblTextsTemplate,
				lblTextsTemplateText,

				lblStatisticsTemplate,
				lblStatisticsTemplateOverview,

				btnImport,
				btnExport

				);

		TabUtils.fillTab(tabOther, this);
		TabUtils.fillTab(tabRefereeReports, this);
		TabUtils.fillTab(tabPaths, this);
		TabUtils.fillTab(tabEMail, this);
		TabUtils.fillTab(tabLetters, this);
		TabUtils.fillTab(tabDocuments, this);
		TabUtils.fillTab(tabTexts, this);
		TabUtils.fillTab(tabStatistics, this);

		btnPathsTemplate.setGraphic(new ImageView(Resources.loadImage("icons/actions/folder-open.svg", Prefs.SIZE_BUTTON_SMALL)));
		btnPathsImage.setGraphic(new ImageView(Resources.loadImage("icons/actions/folder-open.svg", Prefs.SIZE_BUTTON_SMALL)));
		btnPathsOutput.setGraphic(new ImageView(Resources.loadImage("icons/actions/folder-open.svg", Prefs.SIZE_BUTTON_SMALL)));
		btnPathsXSD.setGraphic(new ImageView(Resources.loadImage("icons/actions/document-open.svg", Prefs.SIZE_BUTTON_SMALL)));

		btnRefereeReportPath.setGraphic(new ImageView(Resources.loadImage("icons/actions/folder-open.svg", Prefs.SIZE_BUTTON_SMALL)));

		// spinners
        SpinnerUtils.prepareIntegerSpinner(spnGeoDecimalPlaces, 3, 10);
        SpinnerUtils.prepareIntegerSpinner(spnEMailPauseDuration, 0, 60);
        SpinnerUtils.prepareIntegerSpinner(spnEMailPauseFrequency, 1, 100);

        // prompt texts
		txtRefereeReportPath.setPromptText(PrefKey.REFEREE_REPORT_PATH.defaultValue());
		txtRefereeReportLeagueGame.setPromptText(PrefKey.REFEREE_REPORT_LEAGUE_GAMES.defaultValue());
		txtRefereeReportTournament.setPromptText(PrefKey.REFEREE_REPORT_TOURNAMENTS.defaultValue());

		// fill with existing values
		fillValues();

	}

	/**
	 * Initializes the controller with things, that cannot be done during {@link #initialize()}.
	 *
	 * @param theStage dialog stage
	 * @param theTabID tab to open
	 */
	public void initController(
			final Stage theStage,
			final String theTabID
			) {

		dialogStage = theStage;

		if (theTabID != null) {
			pneTabs.getTabs().forEach(tab -> {
				if (tab.getId().equals(theTabID)) {
					pneTabs.getSelectionModel().select(tab);
				}
			});
		}

	}

	/**
	 * Returns if user clicked ok.
	 *
	 * @return did user click ok?
	 */
	public boolean isOkClicked() {
		return okClicked;
	}

	/**
	 * Fill preference values.
	 */
	private void fillValues() {

		// tab other
		chkTitleFullpath.setSelected(LocalPrefs.getBoolean(PrefKey.OTHER_TITLE_FULLPATH));
		chkDataSortLoading.setSelected(LocalPrefs.getBoolean(PrefKey.OTHER_DATA_SORT_LOADING));
		sldButtonSize.setValue(LocalPrefs.getDouble(PrefKey.SIZE_FACTOR));
		spnGeoDecimalPlaces.getValueFactory().setValue(LocalPrefs.getInt(PrefKey.GEO_COORDS_DECIMAL_PLACES));

		// tab templates
		txtRefereeReportPath.setText(LocalPrefs.get(PrefKey.REFEREE_REPORT_PATH));
		txtRefereeReportLeagueGame.setText(LocalPrefs.get(PrefKey.REFEREE_REPORT_LEAGUE_GAMES));
		txtRefereeReportTournament.setText(LocalPrefs.get(PrefKey.REFEREE_REPORT_TOURNAMENTS));

		// tab paths
		txtPathsImage.setText(LocalPrefs.get(PrefKey.PATHS_IMAGE));
		txtPathsOutput.setText(LocalPrefs.get(PrefKey.PATHS_OUTPUT));
		txtPathsTemplate.setText(LocalPrefs.get(PrefKey.PATHS_TEMPLATE));
		txtPathsXSD.setText(LocalPrefs.get(PrefKey.PATHS_XSD));

		// tab email
		txtEMailSMTPHost.setText(LocalPrefs.get(PrefKey.EMAIL_SMTP_HOST));
		txtEMailSMTPUsername.setText(LocalPrefs.get(PrefKey.EMAIL_SMTP_USERNAME));
		txtEMailSMTPPassword.setText(LocalPrefs.get(PrefKey.EMAIL_SMTP_PASSWORD));
		txtEMailFromName.setText(LocalPrefs.get(PrefKey.EMAIL_FROM_NAME));
		txtEMailFromEMail.setText(LocalPrefs.get(PrefKey.EMAIL_FROM_EMAIL));
		txtEMailTemplateEMail.setText(LocalPrefs.get(PrefKey.EMAIL_TEMPLATE_EMAIL));
		spnEMailPauseDuration.getValueFactory().setValue(LocalPrefs.getInt(PrefKey.EMAIL_PAUSE_DURATION));
		spnEMailPauseFrequency.getValueFactory().setValue(LocalPrefs.getInt(PrefKey.EMAIL_PAUSE_FREQUENCY));
		txtEMailReplyToName1.setText(LocalPrefs.get(PrefKey.EMAIL_REPLY_TO_NAME1));
		txtEMailReplyToEMail1.setText(LocalPrefs.get(PrefKey.EMAIL_REPLY_TO_EMAIL1));
		txtEMailReplyToName2.setText(LocalPrefs.get(PrefKey.EMAIL_REPLY_TO_NAME2));
		txtEMailReplyToEMail2.setText(LocalPrefs.get(PrefKey.EMAIL_REPLY_TO_EMAIL2));

		// tab letters
		txtLettersTemplateLetter.setText(LocalPrefs.get(PrefKey.LETTERS_TEMPLATE_LETTER));
		txtLettersTemplateMergeSingle.setText(LocalPrefs.get(PrefKey.LETTERS_TEMPLATE_MERGE_SINGLE));
		txtLettersTemplateMergeAll.setText(LocalPrefs.get(PrefKey.LETTERS_TEMPLATE_MERGE_ALL));

		// tab documents
		txtDocumentsTemplateDocument.setText(LocalPrefs.get(PrefKey.DOCUMENTS_TEMPLATE_DOCUMENT));

		// tab texts
		txtTextsTemplateText.setText(LocalPrefs.get(PrefKey.TEXTS_TEMPLATE_TEXT));

		// tab statistics
		txtStatisticsTemplateOverview.setText(LocalPrefs.get(PrefKey.STATISTICS_TEMPLATE_OVERVIEW));

	}

	/**
	 * Validates input, stores ok click, and closes dialog; does nothing for invalid input.
	 */
	@FXML
	private void handleOk() {
		okClicked = false;
		if (isInputValid()) {

			// tab other
			LocalPrefs.putBoolean(PrefKey.OTHER_TITLE_FULLPATH, chkTitleFullpath.isSelected());
			LocalPrefs.putBoolean(PrefKey.OTHER_DATA_SORT_LOADING, chkDataSortLoading.isSelected());
			LocalPrefs.putDouble(PrefKey.SIZE_FACTOR, sldButtonSize.getValue());
			Resources.setScalingFactor(LocalPrefs.getDouble(PrefKey.SIZE_FACTOR));
			LocalPrefs.putInt(PrefKey.GEO_COORDS_DECIMAL_PLACES, spnGeoDecimalPlaces.getValue());

			// tab referee report
			LocalPrefs.put(PrefKey.REFEREE_REPORT_PATH, txtRefereeReportPath.getText());
			LocalPrefs.put(PrefKey.REFEREE_REPORT_LEAGUE_GAMES, txtRefereeReportLeagueGame.getText());
			LocalPrefs.put(PrefKey.REFEREE_REPORT_TOURNAMENTS, txtRefereeReportTournament.getText());

			// tab paths
			LocalPrefs.put(PrefKey.PATHS_IMAGE, txtPathsImage.getText());
			LocalPrefs.put(PrefKey.PATHS_OUTPUT, txtPathsOutput.getText());
			LocalPrefs.put(PrefKey.PATHS_TEMPLATE, txtPathsTemplate.getText());
			LocalPrefs.put(PrefKey.PATHS_XSD, txtPathsXSD.getText());

			// tab email
			LocalPrefs.put(PrefKey.EMAIL_SMTP_HOST, txtEMailSMTPHost.getText());
			LocalPrefs.put(PrefKey.EMAIL_SMTP_USERNAME, txtEMailSMTPUsername.getText());
			LocalPrefs.put(PrefKey.EMAIL_SMTP_PASSWORD, txtEMailSMTPPassword.getText());
			LocalPrefs.put(PrefKey.EMAIL_FROM_NAME, txtEMailFromName.getText());
			LocalPrefs.put(PrefKey.EMAIL_FROM_EMAIL, txtEMailFromEMail.getText());
			LocalPrefs.put(PrefKey.EMAIL_TEMPLATE_EMAIL, txtEMailTemplateEMail.getText());
			LocalPrefs.putInt(PrefKey.EMAIL_PAUSE_DURATION, spnEMailPauseDuration.getValue());
			LocalPrefs.putInt(PrefKey.EMAIL_PAUSE_FREQUENCY, spnEMailPauseFrequency.getValue());
			LocalPrefs.put(PrefKey.EMAIL_REPLY_TO_NAME1, txtEMailReplyToName1.getText());
			LocalPrefs.put(PrefKey.EMAIL_REPLY_TO_EMAIL1, txtEMailReplyToEMail1.getText());
			LocalPrefs.put(PrefKey.EMAIL_REPLY_TO_NAME2, txtEMailReplyToName2.getText());
			LocalPrefs.put(PrefKey.EMAIL_REPLY_TO_EMAIL2, txtEMailReplyToEMail2.getText());

			// tab letters
			LocalPrefs.put(PrefKey.LETTERS_TEMPLATE_LETTER, txtLettersTemplateLetter.getText());
			LocalPrefs.put(PrefKey.LETTERS_TEMPLATE_MERGE_SINGLE, txtLettersTemplateMergeSingle.getText());
			LocalPrefs.put(PrefKey.LETTERS_TEMPLATE_MERGE_ALL, txtLettersTemplateMergeAll.getText());

			// tab documents
			LocalPrefs.put(PrefKey.DOCUMENTS_TEMPLATE_DOCUMENT, txtDocumentsTemplateDocument.getText());

			// tab documents
			LocalPrefs.put(PrefKey.TEXTS_TEMPLATE_TEXT, txtTextsTemplateText.getText());

			// tab statistics
			LocalPrefs.put(PrefKey.STATISTICS_TEMPLATE_OVERVIEW, txtStatisticsTemplateOverview.getText());

			okClicked = true;
			dialogStage.close();
		}
	}

	/**
	 * Validates input, shows error message for invalid input.
	 *
	 * @return is input valid?
	 */
	private boolean isInputValid() {

		StringBuilder sbErrorMessage = new StringBuilder();

		if (!txtPathsTemplate.getText().isEmpty()) {
			File flePath = Paths.get(txtPathsTemplate.getText()).toFile();
			if (!flePath.exists() || !flePath.isDirectory()) {
				sbErrorMessage.append("Der Template-Pfad existiert nicht oder ist kein Verzeichnis.\n");
			}
		}

		if (!txtPathsImage.getText().isEmpty()) {
			File flePath = Paths.get(txtPathsImage.getText()).toFile();
			if (!flePath.exists() || !flePath.isDirectory()) {
				sbErrorMessage.append("Der Bilder-Pfad existiert nicht oder ist kein Verzeichnis.\n");
			}
		}

		if (sbErrorMessage.length() == 0) {
				return true;
		}

		// Show the error message.
		AlertUtils.createAlert(AlertType.ERROR, dialogStage,
				"Ungültige Eingaben",
				"Bitte korrigieren Sie die fehlerhaften Eingaben.",
				sbErrorMessage.toString())
		.showAndWait();

		return false;

	}

	/**
	 * Stores non-ok click and closes dialog.
	 */
	@FXML
	private void handleCancel() {
		okClicked = false;
		dialogStage.close();
	}

	/**
	 * Set template path.
	 */
	@FXML
	private void handlePathsTemplate() {
		selectDirectory("Template-Pfad auswählen", PrefKey.PATHS_TEMPLATE, txtPathsTemplate);
	}

	/**
	 * Set image path.
	 *
	 * @since 0.10.0
	 */
	@FXML
	private void handlePathsImage() {
		selectDirectory("Bilder-Pfad auswählen", PrefKey.PATHS_IMAGE, txtPathsImage);
	}

	/**
	 * Set output path.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private void handlePathsOutput() {
		selectDirectory("Ausgabe-Pfad auswählen", PrefKey.PATHS_OUTPUT, txtPathsOutput);
	}

	/**
	 * Set referee report path.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private void handleRefereeReportPath() {
		selectDirectory("OSR-Bericht-Pfad auswählen", PrefKey.REFEREE_REPORT_PATH, txtRefereeReportPath);
	}

	/**
	 * Get directory, set text field.
	 *
	 * @since 0.15.0
	 */
	private void selectDirectory(final String theTitle, final PrefKey thePrefKey, final TextField theField) {

		DirectoryChooser dirChooser = new DirectoryChooser();

		dirChooser.setTitle(theTitle);

		if (!theField.getText().trim().isEmpty()) {
			dirChooser.setInitialDirectory(new File(theField.getText().trim()));
		} else if (!LocalPrefs.get(thePrefKey).isEmpty()) {
			dirChooser.setInitialDirectory(new File(LocalPrefs.get(thePrefKey)));
		}

		File dir = dirChooser.showDialog(dialogStage);

		if (dir != null) {
			theField.setText(dir.getPath());
		}

	}

	/**
	 * Set XSD path.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private void handlePathsXSD() {

		FileChooser fileChooser = new FileChooser();

		fileChooser.setTitle("XML-Schema auswählen");
		fileChooser.getExtensionFilters().addAll(
				new FileChooser.ExtensionFilter("XML-Schema (*.xsd)", "*.xsd"),
				new FileChooser.ExtensionFilter("Alle Dateien (*.*)", "*.*")
				);
		if (!LocalPrefs.get(PrefKey.PATHS_XSD).isEmpty()) {
			Path pathPrefs = Paths.get(LocalPrefs.get(PrefKey.PATHS_XSD));
			fileChooser.setInitialDirectory(pathPrefs.getParent().toFile());
			fileChooser.setInitialFileName(pathPrefs.getFileName().toString());
		}

		File flePrefs = fileChooser.showOpenDialog(dialogStage);

		if (flePrefs != null) {
			txtPathsXSD.setText(flePrefs.getAbsolutePath());
		}

	}

	/**
	 * Imports preferences.
	 */
	@FXML
	private void handleImport() {

		Alert alert = AlertUtils.createAlert(AlertType.CONFIRMATION, dialogStage,
				"Einstellungsimport",
				"Überschreiben von Einstellungen.",
				"Wenn Sie Einstellungen importieren, werden die bisherigen Einstellungen überschrieben, wenn Einstellungen dafür vorhanden sind.\nWollen Sie fortfahren?");

		alert.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);

		Optional<ButtonType> result = alert.showAndWait();
		if (result.isPresent()) {
			if (result.get() == ButtonType.YES) {
				FileChooser fileChooser = new FileChooser();

				fileChooser.setTitle("Einstellungen importieren");
				fileChooser.getExtensionFilters().addAll(
						new FileChooser.ExtensionFilter("Referee-Manager-Einstellungen (*.prefs)", "*.prefs"),
						new FileChooser.ExtensionFilter("Alle Dateien (*.*)", "*.*")
						);
				if (!LocalPrefs.get(PrefKey.PREFERENCES_FILE).isEmpty()) {
					Path pathPrefs = Paths.get(LocalPrefs.get(PrefKey.PREFERENCES_FILE));
					fileChooser.setInitialDirectory(pathPrefs.getParent().toFile());
					fileChooser.setInitialFileName(pathPrefs.getFileName().toString());
				}

				File flePrefs = fileChooser.showOpenDialog(dialogStage);

				if (flePrefs != null) {
					LocalPrefs.put(PrefKey.PREFERENCES_FILE, flePrefs.getAbsolutePath());

					try (InputStream stmIn = new FileInputStream(flePrefs)) {
						Prefs.importPrefs(stmIn);
						fillValues();
					} catch (IOException | InvalidPreferencesFormatException | BackingStoreException e) {
						AlertUtils.createAlert(AlertType.ERROR, dialogStage,
								"Importfehler",
								"Beim Import der Einstellungen ist ein Fehler aufgetreten.",
								MessageFormat.format("{0}\nDie Daten wurden nicht importiert.", e.getMessage()))
						.showAndWait();
					}
				}

			}
		}

	}

	/**
	 * Exports preferences.
	 */
	@FXML
	private void handleExport() {

		FileChooser fileChooser = new FileChooser();

		fileChooser.setTitle("Einstellungen exportieren");
		fileChooser.getExtensionFilters().addAll(
				new FileChooser.ExtensionFilter("Referee-Manager-Einstellungen (*.prefs)", "*.prefs"),
				new FileChooser.ExtensionFilter("Alle Dateien (*.*)", "*.*")
				);
		if (!LocalPrefs.get(PrefKey.PREFERENCES_FILE).isEmpty()) {
			Path pathPrefs = Paths.get(LocalPrefs.get(PrefKey.PREFERENCES_FILE));
			if (pathPrefs.getParent().toFile().exists()) {
				fileChooser.setInitialDirectory(pathPrefs.getParent().toFile());
			}
			fileChooser.setInitialFileName(pathPrefs.getFileName().toString());
		}

		File flePrefs = fileChooser.showSaveDialog(dialogStage);

		if (flePrefs != null) {
			if (!flePrefs.getName().contains(".")) {
				flePrefs = new File(String.format("%s.prefs", flePrefs.getPath()));
			}
			LocalPrefs.put(PrefKey.PREFERENCES_FILE, flePrefs.getAbsolutePath());

			try (OutputStream stmOut = new FileOutputStream(flePrefs)) {
				Prefs.exportPrefs(stmOut);
			} catch (IOException | BackingStoreException e) {
				AlertUtils.createAlert(AlertType.ERROR, dialogStage,
						"Exportfehler",
						"Beim Export der Einstellungen ist ein Fehler aufgetreten.",
						MessageFormat.format("{0}\nDie Daten wurden nicht exportiert.", e.getMessage()))
				.showAndWait();
			}
		}

	}

}

/* EOF */
