package de.edgesoft.refereemanager.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import de.edgesoft.edgeutils.EdgeUtilsException;
import de.edgesoft.edgeutils.commons.Info;
import de.edgesoft.edgeutils.commons.ext.ModelClassExt;
import de.edgesoft.edgeutils.files.Resources;
import de.edgesoft.edgeutils.i18n.I18N;
import de.edgesoft.edgeutils.i18n.ResourceType;
import de.edgesoft.edgeutils.javafx.ControlUtils;
import de.edgesoft.edgeutils.javafx.MenuItemUtils;
import de.edgesoft.edgeutils.javafx.SceneUtils;
import de.edgesoft.edgeutils.jaxb.JAXBFiles;
import de.edgesoft.refereemanager.RefereeManager;
import de.edgesoft.refereemanager.controller.overview.IOverviewController;
import de.edgesoft.refereemanager.controller.overview.OverviewClubsController;
import de.edgesoft.refereemanager.controller.overview.OverviewContactTypesController;
import de.edgesoft.refereemanager.controller.overview.OverviewEventDateTypesController;
import de.edgesoft.refereemanager.controller.overview.OverviewLeagueGamesController;
import de.edgesoft.refereemanager.controller.overview.OverviewLeaguesController;
import de.edgesoft.refereemanager.controller.overview.OverviewOtherEventsController;
import de.edgesoft.refereemanager.controller.overview.OverviewPeopleController;
import de.edgesoft.refereemanager.controller.overview.OverviewRefereeAssignmentTypesController;
import de.edgesoft.refereemanager.controller.overview.OverviewRefereesController;
import de.edgesoft.refereemanager.controller.overview.OverviewRolesController;
import de.edgesoft.refereemanager.controller.overview.OverviewSexTypesController;
import de.edgesoft.refereemanager.controller.overview.OverviewStatusTypesController;
import de.edgesoft.refereemanager.controller.overview.OverviewTournamentsController;
import de.edgesoft.refereemanager.controller.overview.OverviewTraineesController;
import de.edgesoft.refereemanager.controller.overview.OverviewTrainingLevelTypesController;
import de.edgesoft.refereemanager.controller.overview.OverviewVenuesController;
import de.edgesoft.refereemanager.jaxb.Content;
import de.edgesoft.refereemanager.jaxb.ObjectFactory;
import de.edgesoft.refereemanager.model.AppModel;
import de.edgesoft.refereemanager.model.ContentModel;
import de.edgesoft.refereemanager.utils.AlertUtils;
import de.edgesoft.refereemanager.utils.LocalPrefs;
import de.edgesoft.refereemanager.utils.PrefKey;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Controller for application layout.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.10.0
 */
public class AppLayoutController {

	/**
	 * Application icon.
	 */
	public static final Image ICON = Resources.loadImage(I18N.getText("refman.icon").get());

	/**
	 * App border pane.
	 */
	@FXML
	private BorderPane appPane;

	/**
	 * Menu program.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private Menu mnuProgram;

	/**
	 * Menu item program -> preferences.
	 */
	@FXML
	private MenuItem mnuProgramPreferences;

	/**
	 * Menu item program -> quit.
	 */
	@FXML
	private MenuItem mnuProgramQuit;

	/**
	 * Menu file.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private Menu mnuFile;

	/**
	 * Menu item file -> new.
	 */
	@FXML
	private MenuItem mnuFileNew;

	/**
	 * Menu item file -> open.
	 */
	@FXML
	private MenuItem mnuFileOpen;

	/**
	 * Menu item file -> save.
	 */
	@FXML
	private MenuItem mnuFileSave;

	/**
	 * Menu item file -> save as.
	 */
	@FXML
	private MenuItem mnuFileSaveAs;

	/**
	 * Menu people.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private Menu mnuPeople;

	/**
	 * Menu item referee -> overview.
	 */
	@FXML
	private MenuItem mnuOverviewReferees;

	/**
	 * Menu item overview -> people.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private MenuItem mnuOverviewPeople;

	/**
	 * Menu item trainee -> overview.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private MenuItem mnuOverviewTrainees;

	/**
	 * Menu item communication -> referees.
	 */
	@FXML
	private MenuItem mnuCommunicationReferees;

	/**
	 * Menu item communication -> people.
	 *
	 * @since 0.16.0
	 */
	@FXML
	private MenuItem mnuCommunicationPeople;

	/**
	 * Menu item communication -> trainees.
	 *
	 * @since 0.16.0
	 */
	@FXML
	private MenuItem mnuCommunicationTrainees;

	/**
	 * Split menu item referee -> overview.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private MenuItem mnuSplitOverviewReferees;

	/**
	 * Split menu item overview -> people.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private MenuItem mnuSplitOverviewPeople;

	/**
	 * Split menu item trainee -> overview.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private MenuItem mnuSplitOverviewTrainees;

	/**
	 * Split menu item communication -> referees.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private MenuItem mnuSplitCommunicationReferees;

	/**
	 * Split menu item communication -> people.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private MenuItem mnuSplitCommunicationPeople;

	/**
	 * Split menu item communication -> trainees.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private MenuItem mnuSplitCommunicationTrainees;


	/**
	 * Menu events.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private Menu mnuEvents;

	/**
	 * Menu league game overview.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private MenuItem mnuOverviewLeagueGames;

	/**
	 * Menu item tournament overview.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private MenuItem mnuOverviewTournaments;

	/**
	 * Menu item other event overview.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private MenuItem mnuOverviewOtherEvents;

	/**
	 * Menu item other event date types.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private MenuItem mnuOverviewEventDateTypes;


	/**
	 * Menu things.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private Menu mnuThings;

	/**
	 * Menu item club overview.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private MenuItem mnuOverviewClubs;

	/**
	 * Menu item leagues overview.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private MenuItem mnuOverviewLeagues;

	/**
	 * Menu item venues overview.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private MenuItem mnuOverviewVenues;

	/**
	 * Menu item sex types overview.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private MenuItem mnuOverviewSexTypes;

	/**
	 * Menu item roles overview.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private MenuItem mnuOverviewRoles;

	/**
	 * Menu item contact types overview.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private MenuItem mnuOverviewContactTypes;

	/**
	 * Menu item status types overview.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private MenuItem mnuOverviewStatusTypes;

	/**
	 * Menu item training level types overview.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private MenuItem mnuOverviewTrainingLevelTypes;

	/**
	 * Menu item referee assignment types overview.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private MenuItem mnuOverviewRefereeAssignmentTypes;


	/**
	 * Menu statistics.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private Menu mnuStatistics;

	/**
	 * Menu item statistics -> data.
	 */
	@FXML
	private MenuItem mnuStatisticsData;


	/**
	 * Menu help.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private Menu mnuHelp;

	/**
	 * Menu item help -> known issues.
	 */
	@FXML
	private MenuItem mnuHelpKnownIssues;

	/**
	 * Menu item help -> about.
	 */
	@FXML
	private MenuItem mnuHelpAbout;

	/**
	 * Main toolbar.
	 */
	@FXML
	private ToolBar barMain;

	/**
	 * Button program -> quit.
	 */
	@FXML
	private Button btnProgramQuit;

	/**
	 * Button file -> open.
	 */
	@FXML
	private Button btnFileOpen;

	/**
	 * Button file -> save.
	 */
	@FXML
	private Button btnFileSave;

	/**
	 * Button referee -> overview.
	 */
	@FXML
	private SplitMenuButton btnOverviewReferees;


	/**
	 * Button communication -> referees.
	 */
	@FXML
	private SplitMenuButton btnCommunicationReferees;


	/**
	 * Button league games overview.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private Button btnOverviewLeagueGames;

	/**
	 * Button tournament overview.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private Button btnOverviewTournaments;

	/**
	 * Button other event overview.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private Button btnOverviewOtherEvents;

	/**
	 * Button club overview.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private Button btnOverviewClubs;

	/**
	 * Button league overview.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private Button btnOverviewLeagues;

	/**
	 * Button league venues.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private Button btnOverviewVenues;

	/**
	 * Button statistics -> data.
	 */
	@FXML
	private Button btnStatisticsData;

	/**
	 * Button program -> preferences.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private Button btnProgramPreferences;


	/**
	 * Primary stage = window.
	 */
	private Stage window = null;

	/**
	 * Singleton for data table views.
	 */
	private static Map<String, Map.Entry<Parent, FXMLLoader>> snglDataTableViews = null;


	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 *
	 * @since 0.12.0
	 */
	@FXML
	private void initialize() {

		// icons
		MenuItemUtils.fillMenuItem(mnuProgram, this, ResourceType.TEXT);
		MenuItemUtils.fillMenuItem(mnuProgramPreferences, this);
		ControlUtils.fillViewControl(btnProgramPreferences, this, ResourceType.ICON);
		MenuItemUtils.fillMenuItem(mnuProgramQuit, this);
		ControlUtils.fillViewControl(btnProgramQuit, this, ResourceType.ICON);

		MenuItemUtils.fillMenuItem(mnuFile, this, ResourceType.TEXT);
		MenuItemUtils.fillMenuItem(mnuFileNew, this);
		MenuItemUtils.fillMenuItem(mnuFileOpen, this);
		ControlUtils.fillViewControl(btnFileOpen, this, ResourceType.ICON);
		MenuItemUtils.fillMenuItem(mnuFileSave, this);
		ControlUtils.fillViewControl(btnFileSave, this, ResourceType.ICON);
		MenuItemUtils.fillMenuItem(mnuFileSaveAs, this);

		MenuItemUtils.fillMenuItem(mnuPeople, this, ResourceType.TEXT);
		MenuItemUtils.fillMenuItem(mnuOverviewReferees, this);
		ControlUtils.fillViewControl(btnOverviewReferees, this, ResourceType.ICON);
		MenuItemUtils.fillMenuItem(mnuOverviewPeople, this);
		MenuItemUtils.fillMenuItem(mnuOverviewTrainees, this);
		MenuItemUtils.fillMenuItem(mnuCommunicationReferees, this);
		ControlUtils.fillViewControl(btnCommunicationReferees, this, ResourceType.ICON);
		MenuItemUtils.fillMenuItem(mnuCommunicationPeople, this);
		MenuItemUtils.fillMenuItem(mnuCommunicationTrainees, this);
		MenuItemUtils.fillMenuItem(mnuSplitOverviewReferees, this);
		ControlUtils.fillViewControl(btnOverviewReferees, this, ResourceType.ICON);
		MenuItemUtils.fillMenuItem(mnuSplitOverviewTrainees, this);
		MenuItemUtils.fillMenuItem(mnuSplitOverviewPeople, this);
		MenuItemUtils.fillMenuItem(mnuSplitCommunicationReferees, this);
		MenuItemUtils.fillMenuItem(mnuSplitCommunicationPeople, this);
		MenuItemUtils.fillMenuItem(mnuSplitCommunicationTrainees, this);

		MenuItemUtils.fillMenuItem(mnuEvents, this, ResourceType.TEXT);
		MenuItemUtils.fillMenuItem(mnuOverviewLeagueGames, this);
		ControlUtils.fillViewControl(btnOverviewLeagueGames, this, ResourceType.ICON);
		MenuItemUtils.fillMenuItem(mnuOverviewTournaments, this);
		ControlUtils.fillViewControl(btnOverviewTournaments, this, ResourceType.ICON);
		MenuItemUtils.fillMenuItem(mnuOverviewOtherEvents, this);
		ControlUtils.fillViewControl(btnOverviewOtherEvents, this, ResourceType.ICON);
		MenuItemUtils.fillMenuItem(mnuOverviewEventDateTypes, this, ResourceType.ICON, ResourceType.TEXT);

		MenuItemUtils.fillMenuItem(mnuThings, this, ResourceType.TEXT);
		MenuItemUtils.fillMenuItem(mnuOverviewClubs, this, ResourceType.ICON, ResourceType.TEXT);
		ControlUtils.fillViewControl(btnOverviewClubs, this, ResourceType.ICON);
		MenuItemUtils.fillMenuItem(mnuOverviewLeagues, this, ResourceType.ICON, ResourceType.TEXT);
		ControlUtils.fillViewControl(btnOverviewLeagues, this, ResourceType.ICON);
		MenuItemUtils.fillMenuItem(mnuOverviewVenues, this, ResourceType.ICON, ResourceType.TEXT);
		ControlUtils.fillViewControl(btnOverviewVenues, this, ResourceType.ICON);
		MenuItemUtils.fillMenuItem(mnuOverviewSexTypes, this, ResourceType.ICON, ResourceType.TEXT);
		MenuItemUtils.fillMenuItem(mnuOverviewRoles, this, ResourceType.ICON, ResourceType.TEXT);
		MenuItemUtils.fillMenuItem(mnuOverviewContactTypes, this, ResourceType.ICON, ResourceType.TEXT);
		MenuItemUtils.fillMenuItem(mnuOverviewStatusTypes, this, ResourceType.ICON, ResourceType.TEXT);
		MenuItemUtils.fillMenuItem(mnuOverviewTrainingLevelTypes, this, ResourceType.ICON, ResourceType.TEXT);
		MenuItemUtils.fillMenuItem(mnuOverviewRefereeAssignmentTypes, this, ResourceType.ICON, ResourceType.TEXT);

		MenuItemUtils.fillMenuItem(mnuStatistics, this, ResourceType.TEXT);
		MenuItemUtils.fillMenuItem(mnuStatisticsData, this);
		ControlUtils.fillViewControl(btnStatisticsData, this, ResourceType.ICON);

		MenuItemUtils.fillMenuItem(mnuHelp, this, ResourceType.TEXT);
		MenuItemUtils.fillMenuItem(mnuHelpKnownIssues, this);
		MenuItemUtils.fillMenuItem(mnuHelpAbout, this);

	}

	/**
	 * Initializes the controller with things, that cannot be done during {@link #initialize()}.
	 *
	 * @param thePrimaryStage primary stage
	 */
	public void initController(final Stage thePrimaryStage) {

		window = thePrimaryStage;

		// set icon
		window.getIcons().add(ICON);

		// Show the scene containing the root layout.
		window.setScene(SceneUtils.createScene(appPane, RefereeManager.CSS));
		window.show();

		// resize to last dimensions
		window.setX(LocalPrefs.getDouble(PrefKey.STAGE_X));
		window.setY(LocalPrefs.getDouble(PrefKey.STAGE_Y));

		window.setWidth(LocalPrefs.getDouble(PrefKey.STAGE_WIDTH));
		window.setHeight(LocalPrefs.getDouble(PrefKey.STAGE_HEIGHT));

		window.setMaximized(LocalPrefs.getBoolean(PrefKey.STAGE_MAXIMIZED));

		// if changed, save bounds to preferences
		window.xProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			if (!window.isMaximized()) {
				LocalPrefs.putDouble(PrefKey.STAGE_X, newValue.doubleValue());
			}
		});
		window.widthProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			if (!window.isMaximized()) {
				LocalPrefs.putDouble(PrefKey.STAGE_WIDTH, newValue.doubleValue());
			}
		});
		window.yProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			if (!window.isMaximized()) {
				LocalPrefs.putDouble(PrefKey.STAGE_Y, newValue.doubleValue());
			}
		});
		window.heightProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			if (!window.isMaximized()) {
				LocalPrefs.putDouble(PrefKey.STAGE_HEIGHT, newValue.doubleValue());
			}
		});

		window.maximizedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			LocalPrefs.putBoolean(PrefKey.STAGE_MAXIMIZED, newValue.booleanValue());
		});

		// set handler for close requests (x-button of window)
		window.setOnCloseRequest(event -> {
			event.consume();
			handleProgramExit();
		});

		// finally, we can initialize the data
		initData();

	}

	/**
	 * Initializes the data.
	 */
	private void initData() {

		if (AppModel.getFilename().isEmpty()) {
			newData();
		} else {
			openData(LocalPrefs.get(PrefKey.FILE));
		}

	}

	/**
	 * Creates new data.
	 */
	private void newData() {

		de.edgesoft.refereemanager.jaxb.RefereeManager dtaRefMan = new ObjectFactory().createRefereeManager();

		Info info = new de.edgesoft.edgeutils.commons.ObjectFactory().createInfo();

		info.setCreated(LocalDateTime.now());
		info.setModified(LocalDateTime.now());
		info.setAppversion(RefereeManager.getVersion());
		info.setDocversion(RefereeManager.getVersion());
		info.setCreator(RefereeManager.class.getCanonicalName());

		dtaRefMan.setInfo(info);

		Content content = new ObjectFactory().createContent();
		dtaRefMan.setContent(content);

		AppModel.setData(dtaRefMan);
		AppModel.setFilename(null);
		AppModel.setModified(false);

		setAppTitle();

	}

	/**
	 * Sets the app title.
	 */
	public void setAppTitle() {

		String sFile = LocalPrefs.get(PrefKey.FILE).isEmpty() ?
				"" :
				String.format(" - %s", LocalPrefs.getBoolean(PrefKey.OTHER_TITLE_FULLPATH) ?
						Paths.get(LocalPrefs.get(PrefKey.FILE)).toAbsolutePath().toString() :
						Paths.get(LocalPrefs.get(PrefKey.FILE)).getFileName().toString());

		window.setTitle(String.format("Referee Manager%s%s",
				sFile,
				AppModel.isModified() ? " *" : ""
				));

	}

	/**
	 * Loads data from the given file.
	 *
	 * @param theFilename filename
	 *
	 * @since 0.12.0
	 */
	private void openData(final String theFilename) {

		try {

			de.edgesoft.refereemanager.jaxb.RefereeManager dtaRefMan = JAXBFiles.unmarshal(theFilename, de.edgesoft.refereemanager.jaxb.RefereeManager.class);

			AppModel.setData(dtaRefMan);
			AppModel.setFilename(theFilename);
			AppModel.setModified(false);

			if (LocalPrefs.getBoolean(PrefKey.OTHER_DATA_SORT_LOADING)) {
				((ContentModel) AppModel.getData().getContent()).sortData();
			}

			setAppTitle();

		} catch (EdgeUtilsException e) {

			AlertUtils.createAlert(AlertType.ERROR, window,
					"Datenfehler",
					"Ein Fehler ist beim Laden der Referee-Manager-Daten aufgetreten.",
					MessageFormat.format("{0}\nDas Programm wird ohne Daten fortgeführt.", e.getMessage()))
			.showAndWait();

			newData();

		}

	}

	/**
	 * Program menu preferences.
	 *
	 * Opens the preferences edit dialog.
	 */
	@FXML
	private void handleProgramPreferences() {

		Map.Entry<Parent, FXMLLoader> pneLoad = Resources.loadNode("PreferencesDialog");

		// Create the dialog Stage.
		Stage dialogStage = new Stage();
		dialogStage.initModality(Modality.WINDOW_MODAL);
		dialogStage.initOwner(window);

		dialogStage.setScene(SceneUtils.createScene(pneLoad.getKey(), RefereeManager.CSS));

		// initialize controller
		PreferencesDialogController controller = pneLoad.getValue().getController();
		controller.initController(dialogStage, null);
		dialogStage.setTitle(I18N.getViewNodeText(controller, "title", ResourceType.TEXT).get());

		// Show the dialog and wait until the user closes it
		dialogStage.showAndWait();

		setAppTitle();

	}

	/**
	 * Program menu exit.
	 */
	@FXML
	public void handleProgramExit() {
		if (checkModified()) {
			Platform.exit();
		}
	}

	/**
	 * File menu new.
	 */
	@FXML
	private void handleFileNew() {
		if (checkModified()) {
			newData();
		}
	}

	/**
	 * File menu open.
	 */
	@FXML
	private void handleFileOpen() {

		if (checkModified()) {

			FileChooser fileChooser = new FileChooser();

			fileChooser.setTitle("Referee-Manager-Datei öffnen");
			fileChooser.getExtensionFilters().addAll(
					new FileChooser.ExtensionFilter("Referee-Manager-Dateien (*.refman, *.xml)", "*.refman", "*.xml"),
					new FileChooser.ExtensionFilter("Alle Dateien (*.*)", "*.*")
					);
			if (!LocalPrefs.get(PrefKey.PATH).isEmpty()) {
				Path flePath = Paths.get(LocalPrefs.get(PrefKey.PATH));
				if (flePath.toFile().exists()) {
					fileChooser.setInitialDirectory(flePath.toFile());
				}
			}

			File file = fileChooser.showOpenDialog(window);

			if (file != null) {
				openData(file.getPath());
			}

		}

	}

	/**
	 * File menu save.
	 */
	@FXML
	public void handleFileSave() {
		if (LocalPrefs.get(PrefKey.FILE).isEmpty()) {
			handleFileSaveAs();
		} else {
			saveData(LocalPrefs.get(PrefKey.FILE));
		}
	}

	/**
	 * File menu save as.
	 */
	@FXML
	private void handleFileSaveAs() {

		FileChooser fileChooser = new FileChooser();

		fileChooser.setTitle("Referee-Manager-Datei speichern");
		fileChooser.getExtensionFilters().addAll(
				new FileChooser.ExtensionFilter("Referee-Manager-Dateien (*.refman, *.xml)", "*.refman", "*.xml"),
				new FileChooser.ExtensionFilter("Alle Dateien (*.*)", "*.*")
				);
		if (!LocalPrefs.get(PrefKey.PATH).isEmpty()) {
			fileChooser.setInitialDirectory(new File(LocalPrefs.get(PrefKey.PATH)));
		}

		File file = fileChooser.showSaveDialog(window);

		if (file != null) {
			if (!file.getName().contains(".")) {
				file = new File(String.format("%s.refman", file.getPath()));
			}
			saveData(file.getPath());
		}

	}

	/**
	 * Help menu known issues.
	 */
	@FXML
	private void handleHelpKnownIssues() {

		Alert alert = AlertUtils.createAlert(AlertType.INFORMATION, window,
				"Bekannte Probleme und Anregungen",
				MessageFormat.format("Referee-Manager Version {0}", RefereeManager.getVersion()),
				null
				);

		Map.Entry<Parent, FXMLLoader> pneLoad = Resources.loadNode("help/KnownIssues");
		alert.getDialogPane().contentProperty().set(pneLoad.getKey());

		alert.setGraphic(new ImageView(Resources.loadImage("images/icon-64.svg", 64, 64)));
		alert.showAndWait();

	}

	/**
	 * Help menu about.
	 */
	@FXML
	private void handleHelpAbout() {

		Alert alert = AlertUtils.createAlert(AlertType.INFORMATION, window,
				"Über \"Referee-Manager\"",
				MessageFormat.format("Referee-Manager Version {0}", RefereeManager.getVersion()),
				null
				);

		Map.Entry<Parent, FXMLLoader> pneLoad = Resources.loadNode("help/AboutText");
		alert.getDialogPane().contentProperty().set(pneLoad.getKey());

		alert.setGraphic(new ImageView(Resources.loadImage("images/icon-64.svg", 64, 64)));
		alert.showAndWait();

	}

	/**
	 * Menu referee -> overview.
	 */
	@FXML
	private void handleOverviewReferees() {
		handleOverview(new OverviewRefereesController());
	}

	/**
	 * Menu overview -> people.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private void handleOverviewPeople() {
		handleOverview(new OverviewPeopleController());
	}

	/**
	 * Menu overview -> trainees.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private void handleOverviewTrainees() {
		handleOverview(new OverviewTraineesController());
	}

	/**
	 * Load overview view.
	 *
	 * @param theOverviewController overview controller
	 *
	 * @since 0.15.0
	 */
	private void handleOverview(
			final IOverviewController<? extends ModelClassExt> theOverviewController
			) {

		Map.Entry<Parent, FXMLLoader> pneLoad = Resources.loadNode("overview/OverviewDetails");

		appPane.setCenter(pneLoad.getKey());

		theOverviewController.initController(pneLoad.getValue().getController());

	}

	/**
	 * Returns map of available data table views.
	 *
	 * @return map of available data table views
	 */
	private static Map<String, Map.Entry<Parent, FXMLLoader>> getDataTableViews() {

		if (snglDataTableViews == null) {

			try {

				snglDataTableViews = new HashMap<>();

				Resources.getSortedFileList(String.format("%s/datatables/", Resources.PATH_VIEW), ".fxml")
						.stream()
						.forEach(filename -> {
							String tablename = filename.substring(0, filename.indexOf(".fxml"));
							snglDataTableViews.put(tablename, Resources.loadNode(String.format("datatables/%s", tablename)));
						});

			} catch (IOException e) {
				RefereeManager.logger.error(e);
				snglDataTableViews = Collections.emptyMap();
			}

		}

		return snglDataTableViews;

	}

	/**
	 * Menu communication -> abstract.
	 *
	 * @param theDataTableView view name of data table
	 */
	private void handleCommunicationAbstract(final String theDataTableView) {

		Map.Entry<Parent, FXMLLoader> pneLoad = Resources.loadNode("DocumentGeneration");
		BorderPane pneCommunication = (BorderPane) pneLoad.getKey();

		// Set event overview into the center of root layout.
		appPane.setCenter(pneCommunication);

		// Give the controller access to the app.
		DocumentGenerationController ctlRefCommunication = pneLoad.getValue().getController();
		ctlRefCommunication.initController(this, getDataTableViews(), theDataTableView);

	}

	/**
	 * Menu communication -> referees.
	 */
	@FXML
	private void handleCommunicationReferees() {
		handleCommunicationAbstract("DataTableReferees");
	}

	/**
	 * Menu communication -> people.
	 */
	@FXML
	private void handleCommunicationPeople() {
		handleCommunicationAbstract("DataTablePeople");
	}

	/**
	 * Menu communication -> trainees.
	 */
	@FXML
	private void handleCommunicationTrainees() {
		handleCommunicationAbstract("DataTableTrainees");
	}

	/**
	 * Menu overview -> league games.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private void handleOverviewLeagueGames() {
		handleOverview(new OverviewLeagueGamesController());
	}

	/**
	 * Menu overview -> tournaments.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private void handleOverviewTournaments() {
		handleOverview(new OverviewTournamentsController());
	}

	/**
	 * Menu overview -> other events.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private void handleOverviewOtherEvents() {
		handleOverview(new OverviewOtherEventsController());
	}

	/**
	 * Menu overview -> event date types.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private void handleOverviewEventDateTypes() {
		handleOverview(new OverviewEventDateTypesController());
	}

	/**
	 * Menu things -> clubs.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private void handleOverviewClubs() {
		handleOverview(new OverviewClubsController());
	}

	/**
	 * Menu things -> leagues.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private void handleOverviewLeagues() {
		handleOverview(new OverviewLeaguesController());
	}

	/**
	 * Menu things -> venues.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private void handleOverviewVenues() {
		handleOverview(new OverviewVenuesController());
	}

	/**
	 * Menu things -> sex types.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private void handleOverviewSexTypes() {
		handleOverview(new OverviewSexTypesController());
	}

	/**
	 * Menu things -> roles.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private void handleOverviewRoles() {
		handleOverview(new OverviewRolesController());
	}

	/**
	 * Menu things -> contact types.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private void handleOverviewContactTypes() {
		handleOverview(new OverviewContactTypesController());
	}

	/**
	 * Menu things -> status types.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private void handleOverviewStatusTypes() {
		handleOverview(new OverviewStatusTypesController());
	}

	/**
	 * Menu things -> training level types.
	 *
	 * @since 0.15.0
	 */
	@FXML
	private void handleOverviewTrainingLevelTypes() {
		handleOverview(new OverviewTrainingLevelTypesController());
	}

	/**
	 * Menu things -> referee assignment types.
	 *
	 * @since 0.17.0
	 */
	@FXML
	private void handleOverviewRefereeAssignmentTypes() {
		handleOverview(new OverviewRefereeAssignmentTypesController());
	}

	/**
	 * Menu statistics -> data.
	 *
	 * Shows the data statistics.
	 */
	@FXML
	private void handleStatisticsData() {

		Map.Entry<Parent, FXMLLoader> pneLoad = Resources.loadNode("Statistics");

		// Create the dialog Stage.
		Stage dialogStage = new Stage();
		dialogStage.setTitle("Statistik");
		dialogStage.initModality(Modality.WINDOW_MODAL);
		dialogStage.initOwner(window);

		dialogStage.setScene(SceneUtils.createScene(pneLoad.getKey(), RefereeManager.CSS));

		// Set the events into the controller.
		StatisticsController controller = pneLoad.getValue().getController();
		controller.fillStatistics();
		controller.setDialogStage(dialogStage);

		// Show the dialog and wait until the user closes it
		dialogStage.show();

	}

	/**
	 * Check if data is modified, show corresponding dialog, save data if needed.
	 *
	 * @return did user select continue (true) or cancel (false)?
	 */
	private boolean checkModified() {

		boolean doContinue = true;

		if (AppModel.isModified()) {

			Alert alert = AlertUtils.createAlert(AlertType.CONFIRMATION, window,
					"Nicht gespeicherte Änderungen",
					"Sie haben Änderungen durchgeführt, die noch nicht gespeichert wurden.",
					"Wollen Sie die geänderten Daten speichern, nicht speichern oder wollen Sie den gesamten Vorgang abbrechen?");

			alert.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);

			Optional<ButtonType> result = alert.showAndWait();
			if (result.isPresent()) {
				if (result.get() == ButtonType.YES) {
					handleFileSave();
					doContinue = true;
				}
				if (result.get() == ButtonType.NO) {
					doContinue = true;
				}
				if (result.get() == ButtonType.CANCEL) {
					doContinue = false;
				}
			}

		}

		return doContinue;

	}

	/**
	 * Saves the data.
	 *
	 * @param theFilename filename
	 */
	private void saveData(final String theFilename) {

		try {

			AppModel.getData().getInfo().setModified(LocalDateTime.now());
			AppModel.getData().getInfo().setAppversion(RefereeManager.getVersion());
			AppModel.getData().getInfo().setDocversion(RefereeManager.getVersion());
			AppModel.getData().getInfo().setCreator(RefereeManager.class.getCanonicalName());

			JAXBFiles.marshal(new ObjectFactory().createRefereemanager(AppModel.getData()), theFilename,
					(LocalPrefs.get(PrefKey.PATHS_XSD).isEmpty() ? null : LocalPrefs.get(PrefKey.PATHS_XSD)));

			AppModel.setFilename(theFilename);
			AppModel.setModified(false);

		} catch (EdgeUtilsException e) {


			AlertUtils.createAlert(AlertType.ERROR, window,
					"Datenfehler",
					"Ein Fehler ist beim Speichern der Referee-Manager-Daten aufgetreten.",
					MessageFormat.format("{0}\nDie Daten wurden nicht gespeichert.", e.getMessage()))
			.showAndWait();

		}

		setAppTitle();

	}

	/**
	 * Returns primary stage.
	 *
	 * @return primary stage
	 */
	public Stage getPrimaryStage() {
		return window;
	}

}

/* EOF */
