package de.edgesoft.refereemanager;

import java.util.Locale;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.edgesoft.edgeutils.commons.Version;
import de.edgesoft.edgeutils.commons.ext.VersionExt;
import de.edgesoft.edgeutils.files.Prefs;
import de.edgesoft.edgeutils.files.Resources;
import de.edgesoft.edgeutils.i18n.I18N;
import de.edgesoft.refereemanager.controller.AppLayoutController;
import de.edgesoft.refereemanager.utils.LocalPrefs;
import de.edgesoft.refereemanager.utils.PrefKey;
import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

/**
 * RefereeManager application.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of Open-TT: Referee Manager.</p>
 *
 * <p>Open-TT: Referee Manager is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * <p>Open-TT: Referee Manager is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 0.10.0
 */
public class RefereeManager extends Application {

	/**
	 * Predefined css.
	 *
	 * @since 0.17.0
	 */
	public static final String[] CSS = new String[] {
			String.format("resources/css/%s.css", RefereeManager.class.getSimpleName().toLowerCase()),
			"resources/css/edgesoft.css"
	};

	/**
	 * Central logger for all classes.
	 */
	public static final Logger logger = LogManager.getLogger(RefereeManager.class.getPackage().getName());

	/**
	 * Main app controller.
	 *
	 * @since 0.15.0
	 */
	private static AppLayoutController appLayoutController = null;


	/**
	 * Starts the application.
	 *
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * The main entry point for all JavaFX applications.
	 * The start method is called after the init method has returned,
	 * and after the system is ready for the application to begin running.
	 *
	 * @param primaryStage primary stage
	 */
	@Override
	public void start(Stage primaryStage) {

		// initialize resources
		Prefs.init(this.getClass());
		Resources.init(this.getClass(), logger, LocalPrefs.getDouble(PrefKey.SIZE_FACTOR), Prefs.SIZE_BUTTON);
		I18N.init(this.getClass(), Locale.GERMANY, logger);

		showSplashScreen();

		// load app layout and controller, then delegate control to controller
		getAppLayoutController().initController(primaryStage);

	}

	/**
	 * Shows splash screen.
	 *
	 * Inspired by https://gist.github.com/jewelsea/2305098
	 */
	public static void showSplashScreen() {

		// Load splash screen.
		Map.Entry<Parent, FXMLLoader> pneLoad = Resources.loadNode("SplashScreen");
		final AnchorPane pane = (AnchorPane) pneLoad.getKey();

		// Create and fill splash screen stage.
		Stage stage = new Stage();
		stage.initModality(Modality.NONE);
		stage.setAlwaysOnTop(true);
		stage.initStyle(StageStyle.TRANSPARENT);

		stage.setScene(new Scene(pane, new Color(1, 1, 1, .5)));
		stage.sizeToScene();
		stage.centerOnScreen();

		// define task, wait 4 seconds, then return null, i.e. succeed
		// if needed, some progress bar output could be defined here
		final Task<Void> splashTask = new Task<>() {
			@Override
			protected Void call() throws Exception {
				Thread.sleep(4000);
				return null;
			}
		};

		// add listener to succeed state of task, then fade out
		splashTask.stateProperty().addListener((observableValue, oldState, newState) -> {
				if (newState == Worker.State.SUCCEEDED) {
						FadeTransition fadeSplash = new FadeTransition(Duration.seconds(1.2), pane);
						fadeSplash.setFromValue(1.0);
						fadeSplash.setToValue(0.0);
						fadeSplash.setOnFinished(actionEvent -> stage.hide());
						fadeSplash.play();
				}
		});

		// show splash screen, then start fading task
		stage.show();
		new Thread(splashTask).start();

	}

	/**
	 * Program and doc version.
	 *
	 * @return version
	 * @since 0.14.0
	 */
	public static Version getVersion() {
		return new VersionExt(Resources.getProjectProperties().getProperty("longversion"));
	}

	/**
	 * Returns app controller (singleton).
	 *
	 * @since 0.15.0
	 */
	public static AppLayoutController getAppLayoutController() {

		if (appLayoutController == null) {
			Map.Entry<Parent, FXMLLoader> pneLoad = Resources.loadNode("AppLayout");
			appLayoutController = (AppLayoutController) pneLoad.getValue().getController();
		}

		return appLayoutController;

	}

}

/* EOF */
