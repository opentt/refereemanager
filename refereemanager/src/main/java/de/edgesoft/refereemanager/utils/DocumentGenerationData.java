
package de.edgesoft.refereemanager.utils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import de.edgesoft.refereemanager.jaxb.RefereeManager;
import de.edgesoft.refereemanager.messagetext.model.MessageTextModel;
import de.edgesoft.refereemanager.model.TitledIDTypeModel;


/**
 * Document generation data class (data store).
 *
 * TODO this would be so much easier in Kotlin!
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.17.0
 */
public class DocumentGenerationData {

	/**
	 * Today.
	 */
	private LocalDateTime today;

	/**
	 * Filled input data.
	 */
	private MessageTextModel filledInputData;

	/**
	 * All referee manager data.
	 */
	private RefereeManager refereeManagerData;

	/**
	 * Selection.
	 */
	private List<? extends TitledIDTypeModel> selection;

	/**
	 * Currently processed data.
	 */
	private TitledIDTypeModel current;

	/**
	 * Preferences.
	 */
	private Map<String, Object> preferences;

	/**
	 * List of filenames for merging letters.
	 */
	private List<String> filenames;


	/**
	 * Disable default constructor from outside use.
	 */
	private DocumentGenerationData() {
		// nothing to do
	}


	/**
	 * Creates new instance.
	 *
	 * @return new instance
	 */
	public static DocumentGenerationData createInstance() {

		DocumentGenerationData newInstance = new DocumentGenerationData();

		return newInstance;

	}


	/**
	 * Returns today's date.
	 *
	 * @return today's date
	 */
	public LocalDateTime getToday() {
		return today;
	}

	/**
	 * Sets today's date.
	 *
	 * @param theToday today
	 * @return current instance
	 */
	public DocumentGenerationData setToday(
			final LocalDateTime theToday
			) {

		today = theToday;
		return this;

	}


	/**
	 * Returns filled input data.
	 *
	 * @return filled input data
	 */
	public MessageTextModel getFilledInputData() {
		return filledInputData;
	}

	/**
	 * Sets filled input data.
	 *
	 * @param theFilledInputData filled input data
	 * @return current instance
	 */
	public DocumentGenerationData setFilledInputData(
			final MessageTextModel theFilledInputData
			) {

		filledInputData = theFilledInputData;
		return this;

	}


	/**
	 * Returns referee manager data.
	 *
	 * @return referee manager data
	 */
	public RefereeManager getRefereeManagerData() {
		return refereeManagerData;
	}

	/**
	 * Sets referee manager data.
	 *
	 * @param theRefereeManagerData referee manager data
	 * @return current instance
	 */
	public DocumentGenerationData setRefereeManagerData(
			final RefereeManager theRefereeManagerData
			) {

		refereeManagerData = theRefereeManagerData;
		return this;

	}


	/**
	 * Returns selection.
	 *
	 * @return selection
	 */
	public List<? extends TitledIDTypeModel> getSelection() {
		return selection;
	}

	/**
	 * Sets selection.
	 *
	 * @param theSelection selection
	 * @return current instance
	 */
	public DocumentGenerationData setSelection(
			final List<? extends TitledIDTypeModel> theSelection
			) {

		selection = theSelection;
		return this;

	}


	/**
	 * Returns current element.
	 *
	 * @return current element
	 */
	public TitledIDTypeModel getCurrent() {
		return current;
	}

	/**
	 * Sets current element.
	 *
	 * @param theCurrent current element
	 * @return current instance
	 */
	public DocumentGenerationData setCurrent(
			final TitledIDTypeModel theCurrent
			) {

		current = theCurrent;
		return this;

	}


	/**
	 * Returns preferences.
	 *
	 * @return preferences
	 */
	public Map<String, Object> getPreferences() {
		return preferences;
	}

	/**
	 * Sets preferences.
	 *
	 * @param thePreferences preferences
	 * @return current instance
	 */
	public DocumentGenerationData setPreferences(
			final Map<String, Object> thePreferences
			) {

		preferences = thePreferences;
		return this;

	}


	/**
	 * Returns filenames.
	 *
	 * @return filenames
	 */
	public List<String> getFilenames() {
		return filenames;
	}

	/**
	 * Sets filenames.
	 *
	 * @param theFilenames filenames
	 * @return current instance
	 */
	public DocumentGenerationData setFilenames(
			final List<String> theFilenames
			) {

		filenames = theFilenames;
		return this;

	}

}

/* EOF */
