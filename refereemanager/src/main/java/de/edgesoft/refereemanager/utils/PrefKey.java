
package de.edgesoft.refereemanager.utils;

import java.util.Locale;

import javafx.stage.Screen;

/**
 * Preference keys.
 *
 * For enums I use the coding style of jaxb, so there will be no inconsistencies.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.8.0
 */
public enum PrefKey {

	AREA_CODE(Integer.toString(30)),

	COUNTRY_CODE(Integer.toString(49)),
	CONTACT_PRIVATE("ContactType.p"),

	DOCUMENTS_TEMPLATE_DOCUMENT("document/document.mmd"),

	EMAIL_FROM_EMAIL,
	EMAIL_FROM_NAME,
	EMAIL_SMTP_HOST,
	EMAIL_SMTP_PASSWORD,
	EMAIL_SMTP_USERNAME,
	EMAIL_TEMPLATE_EMAIL("email/email.mmd"),
	EMAIL_PAUSE_DURATION(Integer.toString(0)),
	EMAIL_PAUSE_FREQUENCY(Integer.toString(50)),
	EMAIL_REPLY_TO_EMAIL1,
	EMAIL_REPLY_TO_NAME1,
	EMAIL_REPLY_TO_EMAIL2,
	EMAIL_REPLY_TO_NAME2,

	FILE,

	FILENAME_PATTERN_DATABASE("%s.xml"),
	FILENAME_PATTERN_REFEREE_DATA("%s.md"),
	FILENAME_PATTERN_REFEREE_MERGE("merge_%s.tex"),
	FILENAME_PATTERN_REFEREES_MERGE("merge_all.tex"),

	GEO_COORDS_DECIMAL_PLACES(Integer.toString(6)),

	LETTERS_TEMPLATE_LETTER("letter/letter.md"),
	LETTERS_TEMPLATE_MERGE_SINGLE("letter/merge_referee.tex"),
	LETTERS_TEMPLATE_MERGE_ALL("letter/merge_referees.tex"),

	LOCAL_TRAININGLEVEL,

	LOCALE(Locale.GERMANY.toLanguageTag()),

	OTHER_TITLE_FULLPATH,
	OTHER_DATA_SORT_LOADING(Boolean.FALSE.toString()),

	OVERVIEW_CLUB_SPLIT(Double.toString(0.6)),
	OVERVIEW_CONTACT_SPLIT(Double.toString(0.6)),
	OVERVIEW_LEAGUE_SPLIT(Double.toString(0.6)),
	OVERVIEW_LEAGUE_GAME_SPLIT(Double.toString(0.6)),
	OVERVIEW_OTHER_EVENT_SPLIT(Double.toString(0.6)),
	OVERVIEW_EVENTDATETYPE_SPLIT(Double.toString(0.6)),
	OVERVIEW_PERSON_SPLIT(Double.toString(0.6)),
	OVERVIEW_REFEREE_SPLIT(Double.toString(0.6)),
	OVERVIEW_REFEREEASSIGNMENTTYPE_SPLIT(Double.toString(0.6)),
	OVERVIEW_ROLETYPE_SPLIT(Double.toString(0.6)),
	OVERVIEW_SEXTYPE_SPLIT(Double.toString(0.6)),
	OVERVIEW_STATUSTYPE_SPLIT(Double.toString(0.6)),
	OVERVIEW_TOURNAMENT_SPLIT(Double.toString(0.6)),
	OVERVIEW_TRAINEE_SPLIT(Double.toString(0.6)),
	OVERVIEW_TRAININGLEVELTYPE_SPLIT(Double.toString(0.6)),
	OVERVIEW_VENUE_SPLIT(Double.toString(0.6)),

	PATH,

	PATHS_IMAGE,
	PATHS_OUTPUT,
	PATHS_TEMPLATE,
	PATHS_XSD,

	PREFERENCES_FILE,

	REFEREE_COMMUNICATION_FILE,
	REFEREE_COMMUNICATION_LAST_ATTACHMENT_PATH,
	REFEREE_COMMUNICATION_SPLIT_0(Double.toString(0.5)),
	REFEREE_COMMUNICATION_SPLIT_1(Double.toString(0.7)),

	REFEREE_REPORT_LEAGUE_GAMES("OSR_%1$s_%2$s_%3$s-%4$s.pdf"),
	REFEREE_REPORT_PATH("/%1$s/OSR-Berichte"),
	REFEREE_REPORT_TOURNAMENTS("OSR_Turnier_%1$s_%2$s.pdf"),

	SIZE_FACTOR(Double.toString(1)),

	STAGE_WIDTH(Double.toString(1200)),
	STAGE_X(Double.toString((Screen.getPrimary().getBounds().getWidth() - 1200) / 2)),
	STAGE_HEIGHT(Double.toString(800)),
	STAGE_Y(Double.toString((Screen.getPrimary().getBounds().getHeight() - 800) / 2)),
	STAGE_MAXIMIZED(Boolean.FALSE.toString()),

	STATISTICS_TEMPLATE_OVERVIEW("statistics/overview.html"),

	TEXTS_TEMPLATE_TEXT("text/text.mmd"),
	;

    private final String value;

    private final String defaultValue;

    PrefKey() {
        value = name().toLowerCase();
        defaultValue = "";
    }

    PrefKey(final String theDefault) {
        value = name().toLowerCase();
        defaultValue = theDefault;
    }

    public String value() {
        return value;
    }

    public static PrefKey fromValue(String v) {
        for (PrefKey c: PrefKey.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

    public String defaultValue() {
        return defaultValue;
    }

}

/* EOF */
