package de.edgesoft.refereemanager.documentgeneration;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

import de.edgesoft.refereemanager.controller.datatables.AbstractDataTableController;
import de.edgesoft.refereemanager.documentgenerationqueue.jaxb.DocumentGenerationJob;
import de.edgesoft.refereemanager.messagetext.jaxb.MessageTextElement;
import de.edgesoft.refereemanager.model.TitledIDTypeModel;
import de.edgesoft.refereemanager.utils.DocumentGenerationResult;
import javafx.concurrent.Task;
import javafx.scene.layout.VBox;

/**
 * Interface for document generation tasks.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.17.0
 */
public interface IDocumentGenerationTask {

	/**
	 * Returns id for this task.
	 *
	 * @return id
	 */
	public String getID();

	/**
	 * Returns title for this task.
	 *
	 * @return title
	 */
	public String getTitle();

	/**
	 * Returns required data features for this generation task.
	 *
	 * @return required data features
	 */
	public Optional<Set<DataFeature>> getRequiredDataFeatures();

	/**
	 * Returns mandatory document data elements for this generation task.
	 *
	 * @return mandatory document data elements
	 */
	public Set<MessageTextElement> getMandatoryDocumentData();

	/**
	 * Returns optional document data elements for this generation task.
	 *
	 * @return optional document data elements
	 */
	public Set<MessageTextElement> getOptionalDocumentData();

	/**
	 * Returns all document data elements for this generation task.
	 *
	 * @return all document data elements
	 */
	public Set<MessageTextElement> getAllDocumentData();

	/**
	 * Returns additional input elements for this task.
	 *
	 * @return additional input elements
	 */
	public Optional<VBox> getAdditionalInputElements();

	/**
	 * Returns document generation task.
	 *
	 * @param theGenerationJob document generation job
	 * @param theRecipientList data table controller with selection
	 * @return document generation task
	 */
	public Task<Map<DocumentGenerationResult, Integer>> getDocumentGenerationTask(
			final DocumentGenerationJob theGenerationJob,
			final AbstractDataTableController<TitledIDTypeModel> theRecipientList
			);

}

/* EOF */
