package de.edgesoft.refereemanager.documentgeneration;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.prefs.BackingStoreException;

import org.apache.logging.log4j.util.Strings;

import de.edgesoft.edgeutils.datetime.DateTimeUtils;
import de.edgesoft.edgeutils.files.FileAccess;
import de.edgesoft.edgeutils.files.Prefs;
import de.edgesoft.edgeutils.freemarker.TemplateHandler;
import de.edgesoft.refereemanager.RefereeManager;
import de.edgesoft.refereemanager.messagetext.jaxb.MessageText;
import de.edgesoft.refereemanager.messagetext.jaxb.MessageTextElement;
import de.edgesoft.refereemanager.messagetext.model.MessageTextModel;
import de.edgesoft.refereemanager.model.AppModel;
import de.edgesoft.refereemanager.model.TitledIDTypeModel;
import de.edgesoft.refereemanager.utils.DocumentGenerationData;
import de.edgesoft.refereemanager.utils.LocalPrefs;
import de.edgesoft.refereemanager.utils.PrefKey;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Utilities for document generation.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.17.0
 */
public abstract class DocumentGenerationUtils {

	/**
	 * Creates document generation data.
	 *
	 * @param theSelection selected items
	 * @param theCurrentData currently processed data, null if there is none
	 *
	 * @return document generation data
	 */
	public static DocumentGenerationData createDocumentGenerationData(
			final List<? extends TitledIDTypeModel> theSelection,
			final TitledIDTypeModel theCurrentData
			) {

		DocumentGenerationData dtaDocumentGeneration = DocumentGenerationData.createInstance()
				.setToday(LocalDateTime.now())
				.setRefereeManagerData(AppModel.getData())
				.setSelection(theSelection)
				.setCurrent(theCurrentData);

		try {
			dtaDocumentGeneration.setPreferences(Prefs.getPrefsMap());
		} catch (BackingStoreException e) {
			RefereeManager.logger.error(e);
			e.printStackTrace();
			return null;
		}

		return dtaDocumentGeneration;

	}

	/**
	 * Fills document generation data.
	 *
	 * @param theDocumentContent document content
	 * @param theSelection selected items
	 * @param theCurrentData currently processed data, null if there is none
	 *
	 * @return document generation data
	 * @throws IOException if string writer fails
	 * @throws TemplateException if template processing fails
	 */
	public static DocumentGenerationData fillDocumentGenerationData(
			final StringProperty theDocumentContent,
			final List<? extends TitledIDTypeModel> theSelection,
			final TitledIDTypeModel theCurrentData
			) throws TemplateException, IOException {

		DocumentGenerationData dtaDocumentGeneration = createDocumentGenerationData(theSelection, theCurrentData);

		dtaDocumentGeneration.setFilledInputData((MessageTextModel) fillInputData(theDocumentContent, dtaDocumentGeneration));

		if (dtaDocumentGeneration.getFilledInputData().getValueFor(MessageTextElement.DATE) == null) {
			dtaDocumentGeneration.getFilledInputData().getValues().put(MessageTextElement.DATE, new SimpleStringProperty(DateTimeUtils.formatDateTime(LocalDateTime.now(), DateTimeFormatter.ISO_LOCAL_DATE_TIME)));
		}

		return dtaDocumentGeneration;

	}

	/**
	 * Fills input data using freemarker template.
	 *
	 * @param theDocumentContent document content
	 * @param theDocumentGenerationData document generation data
	 *
	 * @return filled input data
	 * @throws IOException if string writer fails
	 * @throws TemplateException if template processing fails
	 */
	public static MessageText fillInputData(
			final StringProperty theDocumentContent,
			final DocumentGenerationData theDocumentGenerationData
			) throws TemplateException, IOException {

		return MessageTextModel.fromFileString(processTemplate(theDocumentContent.getValue(), theDocumentGenerationData));

	}

	/**
	 * Processes template content, returns processed result.
	 *
	 * @param theTemplateContent the template text
	 * @param theDocumentGenerationData document generation data
	 *
	 * @return processed template content
	 * @throws IOException if string writer fails
	 * @throws TemplateException if template processing fails
	 */
	public static String processTemplate(
			final String theTemplateContent,
			final DocumentGenerationData theDocumentGenerationData
			) throws TemplateException, IOException {

		return processTemplate(new Template(null, new StringReader(theTemplateContent), TemplateHandler.getConfiguration()), theDocumentGenerationData);

	}

	/**
	 * Processes template, returns processed result.
	 *
	 * @param theTemplate template
	 * @param theDocumentGenerationData document generation data
	 *
	 * @return processed template content
	 * @throws IOException if string writer fails
	 * @throws TemplateException if template processing fails
	 */
	public static String processTemplate(
			final Template theTemplate,
			final DocumentGenerationData theDocumentGenerationData
			) throws TemplateException, IOException {

		String sReturn = null;

		try (StringWriter wrtContent = new StringWriter()) {
			theTemplate.process(theDocumentGenerationData, wrtContent);
			sReturn = wrtContent.toString();
		}

		return sReturn;

	}

	/**
	 * Fills output path.
	 *
	 * @param theOutputPath output path
	 * @param theFilename output filename
	 * @param theDocumentGenerationData document generation data
	 *
	 * @throws IOException if an error occurs
	 * @throws TemplateException if an error occurs
	 */
	public static Path getFilledOutputPath(
			final String theOutputPath,
			final String theFilename,
			final DocumentGenerationData theDocumentGenerationData
			) throws TemplateException, IOException {

		String sOutputPath = Strings.isBlank(theOutputPath) ? "" : theOutputPath.strip();

		return Path.of(
				(sOutputPath.startsWith(File.separator) ? "" : processTemplate(LocalPrefs.get(PrefKey.PATHS_OUTPUT), theDocumentGenerationData)),
				sOutputPath,
				theFilename.strip()
				);

	}

	/**
	 * Loads template.
	 *
	 * @param theTemplateName template name
	 * @return loaded template
	 *
	 * @throws IOException if an error occurs
	 */
	public static Template loadTemplate(
			final String theTemplateName
			) throws IOException {

		Path pathTemplateFile = Paths.get(LocalPrefs.get(PrefKey.PATHS_TEMPLATE), theTemplateName);
		RefereeManager.logger.info(MessageFormat.format("Lade Template ''{0}''.", pathTemplateFile.toAbsolutePath().toString()));

		return TemplateHandler.loadTemplate(pathTemplateFile.getFileName().toString(), pathTemplateFile.getParent().toFile());

	}

	/**
	 * Processes template, returns processed result.
	 *
	 * @param theTemplate template
	 * @param theDocumentGenerationData document generation data
	 * @param theOutputPath output path
	 *
	 * @throws IOException if file writing fails
	 * @throws TemplateException if template processing fails
	 */
	public static void processAndSaveTemplate(
			final Template theTemplate,
			final DocumentGenerationData theDocumentGenerationData,
			final Path theOutputPath
			) throws TemplateException, IOException {

		String sFileContent = processTemplate(theTemplate, theDocumentGenerationData);
		FileAccess.writeFile(theOutputPath, sFileContent);
		RefereeManager.logger.info(MessageFormat.format("Datei ''{0}'' erzeugt", theOutputPath.toAbsolutePath().toString()));

	}

}

/* EOF */
