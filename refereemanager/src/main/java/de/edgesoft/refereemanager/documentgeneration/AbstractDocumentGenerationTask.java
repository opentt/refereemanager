package de.edgesoft.refereemanager.documentgeneration;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.edgesoft.edgeutils.i18n.I18N;
import de.edgesoft.edgeutils.i18n.ResourceType;
import de.edgesoft.edgeutils.javafx.CSSClasses;
import de.edgesoft.refereemanager.messagetext.jaxb.MessageTextElement;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 * Abstract document generation task.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.17.0
 */
public abstract class AbstractDocumentGenerationTask implements IDocumentGenerationTask {

	/**
	 * Id for gridpane.
	 */
	protected final String GRIDPANE_ID = "pneGrid";

	/**
	 * Token for title.
	 */
	private final String TITLE = "title";


	@Override
	public String getID() {
		return getClass().getCanonicalName();
	}

	@Override
	public String getTitle() {
		return I18N.getViewNodeText(this, TITLE, ResourceType.TEXT).get();
	}

	@Override
	public Optional<Set<DataFeature>> getRequiredDataFeatures() {
		return Optional.empty();
	}

	@Override
	public Set<MessageTextElement> getAllDocumentData() {

		return Stream.of(getMandatoryDocumentData(), getOptionalDocumentData())
				.flatMap(Set::stream)
				.collect(Collectors.toSet());

	}

	@Override
	public Optional<VBox> getAdditionalInputElements() {
		return Optional.empty();
	}

	/**
	 * Returns {@link VBox} with heading {@link Label} and {@link GridPane}.
	 *
	 * @return vbox
	 */
	protected VBox getVBoxWithHeadingAndGridPane() {

		VBox boxReturn = new VBox();
		boxReturn.getStyleClass().add("refereemanager-subbox");

		Label lblHeading = new Label();
		lblHeading.setId("lblHeading");
		lblHeading.setText(getTitle());
		lblHeading.getStyleClass().add(CSSClasses.HEADING2.value());
		boxReturn.getChildren().add(lblHeading);

		GridPane pneGrid = new GridPane();
		pneGrid.setId(GRIDPANE_ID);
		pneGrid.getStyleClass().add(CSSClasses.GRIDPANE.value());

//		pneGrid.getColumnConstraints().add(new ColumnConstraints(ColumnConstraints.CONSTRAIN_TO_PREF, 130.0, ColumnConstraints.CONSTRAIN_TO_PREF, Priority.SOMETIMES, null, false));
//		pneGrid.getColumnConstraints().add(new ColumnConstraints(ColumnConstraints.CONSTRAIN_TO_PREF, ColumnConstraints.CONSTRAIN_TO_PREF, ColumnConstraints.CONSTRAIN_TO_PREF, Priority.SOMETIMES, null, true));
//		pneGrid.getColumnConstraints().add(new ColumnConstraints(ColumnConstraints.CONSTRAIN_TO_PREF, ColumnConstraints.CONSTRAIN_TO_PREF, ColumnConstraints.CONSTRAIN_TO_PREF, Priority.NEVER, null, false));
		pneGrid.getColumnConstraints().add(new ColumnConstraints(130.0));
		pneGrid.getColumnConstraints().add(new ColumnConstraints());
		pneGrid.getColumnConstraints().add(new ColumnConstraints());

		boxReturn.getChildren().add(pneGrid);

		return boxReturn;

	}

}

/* EOF */
