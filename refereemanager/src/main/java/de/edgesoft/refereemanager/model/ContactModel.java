package de.edgesoft.refereemanager.model;

import java.util.Objects;
import java.util.function.Predicate;

import de.edgesoft.refereemanager.jaxb.Contact;
import de.edgesoft.refereemanager.utils.PrefKey;
import javafx.beans.property.SimpleBooleanProperty;
import de.edgesoft.refereemanager.utils.LocalPrefs;

/**
 * Contact model, additional methods for jaxb model class.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.8.0
 */
public class ContactModel extends Contact {

	/**
	 * Filter predicate for primary contacts.
	 */
	public static final Predicate<Contact> ISPRIMARY = (contact -> contact.getIsPrimary().get());

	/**
	 * Use private data only?
	 *
	 * This was introduced, because it is far too difficult to provide
	 * the getter methods with a parameter.
	 *
	 * TODO check if there is a better way of implementing this
	 */
	public static boolean isPrivateOnly = false;

	/**
	 * Is contact private?
	 *
	 * @return is contact private
	 */
	public boolean isPrivate() {

		return (getContactType() == null) || ((getContactType() != null) && getContactType().getId().equals(LocalPrefs.get(PrefKey.CONTACT_PRIVATE)));

	}

    /**
     * Gets the value of the isPrimary property.
     *
     * No value means true.
     *
     * @return is contact primary?
     */
	@Override
    public SimpleBooleanProperty getIsPrimary() {
        return Objects.requireNonNullElse(isPrimary, new SimpleBooleanProperty(true));
    }

}

/* EOF */
