package de.edgesoft.refereemanager.model;

import java.util.Comparator;

import de.edgesoft.refereemanager.jaxb.League;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * League model, additional methods for jaxb model class.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.9.0
 */
public class LeagueModel extends League {

	/**
	 * Comparator rank.
	 */
	public static final Comparator<League> RANK = Comparator.comparingInt(league -> league.getRank().get());

	/**
	 * Comparator rank and display title.
	 *
	 * @since 0.10.0
	 */
	public static final Comparator<League> RANK_DISPLAYTITLE = RANK.thenComparing(TitledIDTypeModel.DISPLAYTITLE);

	@Override
	public SimpleBooleanProperty getNational() {
		return (super.getNational() == null) ? new SimpleBooleanProperty(Boolean.FALSE) : super.getNational();
	}

	@Override
    public SimpleIntegerProperty getRank() {
        return (super.getRank() == null) ? new SimpleIntegerProperty(-1) : super.getRank();
    }

	@Override
    public SimpleDoubleProperty getAllowanceDaily() {
        return ((super.getAllowanceDaily() == null) || (super.getAllowanceDaily().getValue() == null)) ? new SimpleDoubleProperty(-1.0) : super.getAllowanceDaily();
    }

	@Override
    public SimpleDoubleProperty getAllowancePerKM() {
        return ((super.getAllowancePerKM() == null) || (super.getAllowancePerKM().getValue() == null)) ? new SimpleDoubleProperty(-1.0) : super.getAllowancePerKM();
    }

}

/* EOF */
