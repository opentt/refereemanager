package de.edgesoft.refereemanager.model;

import de.edgesoft.refereemanager.jaxb.PhoneNumber;
import de.edgesoft.refereemanager.utils.PrefKey;
import de.edgesoft.refereemanager.utils.LocalPrefs;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * PhoneNumber model, additional methods for jaxb model class.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.6.0
 */
public class PhoneNumberModel extends PhoneNumber {

	/**
	 * Returns number (null if private).
	 *
	 * @return number
	 *  @retval null if private number
	 *
	 * @since 0.12.0
	 */
	@Override
	public SimpleStringProperty getNumber() {

		if (isPrivateOnly && !isPrivate()) {
			return null;
		}

		return super.getNumber();

	}

	/**
	 * Returns country code.
	 *
	 * @return country code
	 *
	 * @since 0.12.0
	 */
	@Override
	public SimpleStringProperty getCountryCode() {
		return (super.getCountryCode() == null) ? new SimpleStringProperty(LocalPrefs.get(PrefKey.COUNTRY_CODE)) : super.getCountryCode();
	}

	/**
	 * Returns area code.
	 *
	 * @return area code
	 *
	 * @since 0.12.0
	 */
	@Override
	public SimpleStringProperty getAreaCode() {
		return (super.getAreaCode() == null) ? new SimpleStringProperty(LocalPrefs.get(PrefKey.AREA_CODE)) : super.getAreaCode();
	}

	/**
	 * Display text.
	 *
	 * @return display text
	 */
	@Override
	public SimpleStringProperty getDisplayText() {

		if (getNumber() == null) {
			return null;
		}

		StringBuilder sbReturn = new StringBuilder();

		if (!getCountryCode().getValue().equals(LocalPrefs.get(PrefKey.COUNTRY_CODE))) {
			sbReturn.append(String.format("+%s ", getCountryCode().getValue()));
		}

		sbReturn.append(String.format("0%s %s", getAreaCode().getValue(), getNumber().getValue()));

		if (!isPrivate()) {
			sbReturn.append(String.format(" (%s)", getContactType().getShorttitle().getValue()));
		}

		return new SimpleStringProperty(sbReturn.toString());

	}

	/**
	 * Returns standard representation.
	 *
	 * @return standard representation
	 *
	 * @since 0.12.0
	 */
	public SimpleStringProperty getStandard() {

		if (getNumber() == null) {
			return null;
		}

		return new SimpleStringProperty(String.format("+%s %s %s",
				getCountryCode().getValue(),
				getAreaCode().getValue(),
				getNumber().getValue()));

	}

	/**
	 * Returns vcard representation.
	 *
	 * @return vcard representation
	 *
	 * @since 0.12.0
	 */
	public SimpleStringProperty getVCard() {

		if (getStandard() == null) {
			return null;
		}

		return new SimpleStringProperty(getStandard().getValue().replace(' ', '-'));

	}

	/**
	 * Returns is phone is cell phone.
	 *
	 * @return is cell phone?
	 *
	 * @since 0.12.0
	 */
	@Override
	public SimpleBooleanProperty getIsCell() {
		return (super.getIsCell() == null) ? new SimpleBooleanProperty(false) : super.getIsCell();
	}

}

/* EOF */
