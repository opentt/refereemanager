package de.edgesoft.refereemanager.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import de.edgesoft.edgeutils.datetime.DateTimeUtils;
import de.edgesoft.edgeutils.i18n.I18N;
import de.edgesoft.refereemanager.jaxb.EventDay;
import de.edgesoft.refereemanager.jaxb.Referee;
import de.edgesoft.refereemanager.jaxb.RefereeAssignment;
import de.edgesoft.refereemanager.jaxb.RefereeAssignmentType;
import de.edgesoft.refereemanager.jaxb.RefereeQuantity;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Event day model, additional methods for jaxb model class.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.15.0
 */
public class EventDayModel extends EventDay {

	/**
	 * Returns display text.
	 *
	 * @return display text
	 */
	@Override
	public StringProperty getDisplayText() {
		return getDateTimeText();
	}

	/**
	 * Returns date text.
	 *
	 * @return date text
	 */
	public SimpleStringProperty getDateText() {

		if (getDate() == null) {
			return null;
		}

		return new SimpleStringProperty(DateTimeUtils.formatDate((LocalDate) getDate().getValue()));

	}

	/**
	 * Returns time text.
	 *
	 * @return time text
	 */
	public SimpleStringProperty getTimeText() {

		if ((getStartTime() == null) || (getStartTime().getValue() == null)) {
			return null;
		}

		String sStartTime = DateTimeUtils.formatTime((LocalTime) getStartTime().getValue());

		if ((getEndTime() == null) || (getEndTime().getValue() == null)) {
			return new SimpleStringProperty(I18N.getText("refman.basics.time.single.text", sStartTime).get());
		}

		return new SimpleStringProperty(I18N.getText("refman.basics.time.range.text",
				sStartTime,
				DateTimeUtils.formatTime((LocalTime) getEndTime().getValue())).get());

	}

	/**
	 * Returns referee meeting time text.
	 *
	 * @return referee meeting time text
	 *
	 * @since 0.18.0
	 */
	public SimpleStringProperty getRefereeMeetingTimeText() {

		if ((getRefereeMeetingTime() == null) || (getRefereeMeetingTime().getValue() == null)) {
			return null;
		}

		return new SimpleStringProperty(I18N.getText("refman.basics.time.single.text", DateTimeUtils.formatTime((LocalTime) getRefereeMeetingTime().getValue())).get());

	}

	/**
	 * Returns date and time text.
	 *
	 * @return date and time text
	 */
	public StringProperty getDateTimeText() {
		return (getTimeText() == null) ? getDateText() : new SimpleStringProperty(I18N.getText("refman.basics.datetime.text", getDateText().getValue(), getTimeText().getValue()).get());
	}

	/**
	 * Returns referee quantity text.
	 *
	 * @return referee quantity text
	 */
	public StringProperty getRefereeQuantityText() {

		if (getRefereeQuantity().isEmpty()) {
			return null;
		}

		return new SimpleStringProperty(
				getRefereeQuantity()
						.stream()
						.sorted(RefereeQuantityModel.DEFAULT_COMPARATOR)
						.map(quantity -> quantity.getDisplayText().getValueSafe())
						.collect(Collectors.joining("\n"))
		);
	}

	/**
	 * Returns referee assignments text.
	 *
	 * @return referee assignments text
	 */
	public StringProperty getRefereeAssignmentText() {

		if (getRefereeAssignment().isEmpty()) {
			return null;
		}

		return new SimpleStringProperty(
				getRefereeAssignment()
						.stream()
						.sorted(RefereeAssignmentModel.DEFAULT_COMPARATOR)
						.map(assignment -> assignment.getDisplayText().getValueSafe())
						.collect(Collectors.joining("\n"))
		);
	}

	/**
	 * Returns number of needed referees for given assignment type.
	 *
	 * @param theRefereeAssignmentType assignment type
	 * @return number of needed referees for given assignment type
     *
     * @since 0.17.0
	 */
	public int getNeededRefereeCountFor(
			final RefereeAssignmentType theRefereeAssignmentType
			) {

		Optional<RefereeQuantity> quantity = getRefereeQuantity().stream()
				.filter(refereequantity -> (refereequantity.getRefereeAssignmentType() == theRefereeAssignmentType))
				.findFirst();

		if (quantity.isPresent()) {
			return quantity.get().getQuantity().intValue();
		}

		return 0;

	}

	/**
	 * Returns list of assigned referees for given assignment type.
	 *
	 * @param theRefereeAssignmentType assignment type
	 * @return list of referees for given assignment type
     *
     * @since 0.17.0
	 */
	public List<Referee> getRefereesFor(
			final RefereeAssignmentType theRefereeAssignmentType
			) {

		return getRefereeAssignment().stream()
				.filter(refereeassignment -> (refereeassignment.getRefereeAssignmentType() == theRefereeAssignmentType))
				.map(RefereeAssignment::getReferee)
				.distinct()
				.sorted(PersonModel.DEFAULT_COMPARATOR)
				.collect(Collectors.toUnmodifiableList());

	}

	/**
	 * Returns list of assigned referees for given assignment type.
	 *
	 * @param theRefereeAssignmentType assignment type
	 * @return list of referees for given assignment type
     *
     * @since 0.17.0
	 */
	public List<RefereeAssignment> getRefereeAssignmentsFor(
			final RefereeAssignmentType theRefereeAssignmentType
			) {

		return getRefereeAssignment().stream()
				.filter(refereeassignment -> (refereeassignment.getRefereeAssignmentType() == theRefereeAssignmentType))
				.distinct()
				.sorted(Comparator.comparing(RefereeAssignment::getReferee, PersonModel.DEFAULT_COMPARATOR))
				.collect(Collectors.toUnmodifiableList());

	}

	/**
	 * Returns number of assigned referees for given assignment type.
	 *
	 * @param theRefereeAssignmentType assignment type
	 * @return number of assigned referees for given assignment type
     *
     * @since 0.17.0
	 */
	public int getRefereeCountFor(
			final RefereeAssignmentType theRefereeAssignmentType
			) {

		return getRefereesFor(theRefereeAssignmentType).size();

	}

	/**
	 * Returns full detail text.
	 *
	 * @return full detail text
	 */
	public StringProperty getDetailText() {

		List<String> lstDetails = new ArrayList<>();

		if (getDateTimeText() != null) {
			lstDetails.add(getDateTimeText().getValue());
		}

		if (getRefereeQuantityText() != null) {
			lstDetails.add(getRefereeQuantityText().getValue());
		}

		if (getRefereeAssignmentText() != null) {
			lstDetails.add(getRefereeAssignmentText().getValue());
		}

		return new SimpleStringProperty(String.join("\n", lstDetails));

	}

	/**
	 * Returns if enough referees are assigned.
	 *
	 * @return are enough referees assigned?
     *
     * @since 0.17.0
	 */
	public boolean isEnoughReferees() {

		boolean isEnough = true;

		for (RefereeAssignmentType theAssignmentType : AppModel.getData().getContent().getRefereeAssignmentType()) {

			if (getRefereeCountFor(theAssignmentType) < getNeededRefereeCountFor(theAssignmentType)) {
				isEnough = false;
			}

		}

		return isEnough;

	}

	/**
	 * Returns number of missing assignments for given assignment type.
	 *
	 * @param theRefereeAssignmentType assignment type
	 * @return number of missing assignments for given assignment type
	 *
	 * @since 0.18.0
	 */
	public int getMissingCountFor(
			final RefereeAssignmentType theRefereeAssignmentType
			) {

		int iMissingCount = getNeededRefereeCountFor(theRefereeAssignmentType) - getRefereeCountFor(theRefereeAssignmentType);
		return (iMissingCount < 0) ? 0 : iMissingCount;

	}

}

/* EOF */
