package de.edgesoft.refereemanager.model;

import java.text.MessageFormat;

import de.edgesoft.edgeutils.i18n.I18N;
import de.edgesoft.refereemanager.jaxb.Address;
import de.edgesoft.refereemanager.utils.LocalPrefs;
import de.edgesoft.refereemanager.utils.PrefKey;
import javafx.beans.property.SimpleStringProperty;

/**
 * Address model, additional methods for jaxb model class.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.6.0
 */
public class AddressModel extends Address {

	/**
	 * Display text.
	 *
	 * @return display text
	 */
	@Override
	public SimpleStringProperty getDisplayText() {
		return getDisplayText(false);
	}

	/**
	 * Display text.
	 *
	 * @param showGeolocation show geolocation?
	 * @return display text
	 *
	 * @since 0.17.0
	 */
	public SimpleStringProperty getDisplayText(
			final boolean showGeolocation
			) {

		if (isPrivateOnly && !isPrivate()) {
			return null;
		}

		String sStreet = (getStreet() == null) ? "" : getStreet().getValueSafe();
		String sNumber = (getNumber() == null) ? "" : getNumber().getValueSafe();
		String sZipCode = (getZipCode() == null) ? "" : getZipCode().getValueSafe();
		String sCity = (getCity() == null) ? "" : getCity().getValueSafe();
		String sGeolocation = (getGeolocation() == null) ? "" : getGeolocation().getValueSafe();

		String sStreetNumber = I18N.getText(
				(sNumber.isBlank() ? "model.addressmodel.displaytext.street" : "model.addressmodel.displaytext.streetnumber"),
				sStreet,
				sNumber
				).get();

		String sCityZip = I18N.getText(
				(sZipCode.isBlank() ? "model.addressmodel.displaytext.city" : "model.addressmodel.displaytext.cityzip"),
				sCity,
				sZipCode
				).get();

		String sComplete = I18N.getText(
				"model.addressmodel.displaytext.complete",
				sStreetNumber,
				sCityZip
				).get();

		return new SimpleStringProperty(
				(showGeolocation && !sGeolocation.isBlank()) ?
						I18N.getText("model.addressmodel.displaytext.completegeoloc", sComplete, sGeolocation).get() :
						sComplete
				);

	}

	/**
	 * Geolocation.
	 *
	 * @return geolocation
	 *
	 * @since 0.17.0
	 */
	public SimpleStringProperty getGeolocation() {

		if ((getLatitude() == null) || (getLongitude() == null)) {
			return null;
		}

		return new SimpleStringProperty(MessageFormat.format(String.format("({0,number,#.%1$s},{1,number,#.%1$s})", "#".repeat(LocalPrefs.getInt(PrefKey.GEO_COORDS_DECIMAL_PLACES))),
				(getLatitude() == null) ? 0 : getLatitude().getValue(),
				(getLongitude() == null) ? 0 : getLongitude().getValue()
				));

	}

}

/* EOF */
