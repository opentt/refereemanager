package de.edgesoft.refereemanager.model;

import java.text.MessageFormat;

import de.edgesoft.refereemanager.jaxb.Season;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Season model, additional methods for jaxb model class.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.5.0
 */
public class SeasonModel extends Season {

    /**
     * Returns the end year of the season.
     *
     * @return end year
     *
     * @since 0.15.0
     */
    public SimpleIntegerProperty getEndYear() {
        return new SimpleIntegerProperty(getStartYear().getValue() + 1);
    }

	/**
	 * Gets the value of the title property.
	 *
	 * @return title
	 */
	@Override
	public SimpleStringProperty getTitle() {
		if (title == null) {
			return new SimpleStringProperty(MessageFormat.format("{0,number,####}-{1,number,####}", getStartYear().getValue(), getEndYear().getValue()));
		}
		return title;
	}

}

/* EOF */
