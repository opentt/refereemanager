package de.edgesoft.refereemanager.model;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import de.edgesoft.edgeutils.datetime.DateTimeUtils;
import de.edgesoft.edgeutils.files.FileUtils;
import de.edgesoft.edgeutils.i18n.I18N;
import de.edgesoft.refereemanager.jaxb.EventDate;
import de.edgesoft.refereemanager.jaxb.EventDateType;
import de.edgesoft.refereemanager.jaxb.EventDay;
import de.edgesoft.refereemanager.jaxb.Referee;
import de.edgesoft.refereemanager.jaxb.RefereeAssignmentType;
import de.edgesoft.refereemanager.utils.LocalPrefs;
import de.edgesoft.refereemanager.utils.PrefKey;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Event date model, additional methods for jaxb model class.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.9.0
 */
public abstract class EventDateModel extends EventDate {

    /**
     * Returns filename.
     *
     * @return filename
     *
     * @since 0.17.0
     */
    public SimpleStringProperty getFilename() {
		return new SimpleStringProperty(FileUtils.cleanFilename(
				String.format("%1$s_%2$s",
						(getFirstDay() == null) ? "nodate" : DateTimeUtils.formatDate((LocalDate) getFirstDay().getDate().getValue(), "yyyy-MM-dd"),
						getDisplayTitleShort().getValueSafe()
						)
				));
    }

    /**
     * Returns referee report filename.
     *
     * This method should be abstract but the generated JAXB classes raise errors
     * if it is declared so.
     * Thus, a standard implementation with return "null" is given, which is not
     * the best solution, but it is one.
     *
     * @return referee report filename
     *
     * @since 0.17.0
     */
    @SuppressWarnings("static-method")
	public SimpleStringProperty getRefereeReportFilename() {
    	return null;
    }

	/**
	 * Returns if referee report file exists.
	 *
	 * @return does referee report file exist?
     *
     * @since 0.17.0
	 */
	public SimpleBooleanProperty existsRefereeReportFile() {

		if (
				(getFirstDay() == null) ||
				(getFirstDay().getDate() == null) ||
				(((LocalDate) getFirstDay().getDate().getValue()).isBefore(LocalDate.now()))
				) {

			return new SimpleBooleanProperty(FileUtils.existsFile(
					String.format(LocalPrefs.get(PrefKey.REFEREE_REPORT_PATH), AppModel.getData().getContent().getSeason().getDisplayText().getValueSafe()),
					getRefereeReportFilename()
					));

		}

		return null;

	}

	/**
	 * Returns if enough referees are assigned.
	 *
	 * @return are enough referees assigned?
     *
     * @since 0.17.0
	 */
	public boolean isEnoughReferees() {

		boolean isEnough = true;

		for (EventDay theEventDay : getDay()) {

			isEnough = isEnough && ((EventDayModel) theEventDay).isEnoughReferees();

		}

		return isEnough;

	}

	/**
	 * Returns filter predicate for given type.
	 *
	 * @param theType type
	 * @return predicate
	 *
	 * @since 0.15.0
	 */
	public static Predicate<EventDateModel> getTypePredicate(EventDateType theType) {
		return date -> date.getType() == theType;
	}

	/**
	 * Comparator start date/time.
	 */
	public static final Comparator<EventDateModel> DATE_FIRST = Comparator.comparing(date ->
			((date.getFirstDay() == null) || (date.getFirstDay().getDate() == null)) ? null : (LocalDate) date.getFirstDay().getDate().getValue(),
			Comparator.nullsLast(LocalDate::compareTo)
	);

	/**
	 * Returns date range text.
	 *
	 * @return date range text
	 *
	 * @since 0.15.0
	 */
	public SimpleStringProperty getDateRangeText() {

		if ((getFirstDay() == null) || (getFirstDay().getDate() == null)) {
			return null;
		}

		if ((getLastDay() == null) || (getLastDay().getDate() == null)) {
			return getFirstDay().getDateText();
		}

		return new SimpleStringProperty(I18N.getText("refman.basics.range.text",
				getFirstDay().getDateText().getValue(),
				getLastDay().getDateText().getValue()).get());

	}

	/**
	 * Returns first day.
	 *
	 * @return first day
	 *
	 * @since 0.15.0
	 */
	public EventDayModel getFirstDay() {

		if (getDay().isEmpty()) {
			return null;
		}

		return (EventDayModel) getDay().get(0);

	}

	/**
	 * Returns last day if different from first day.
	 *
	 * @return last day
	 *
	 * @since 0.15.0
	 */
	public EventDayModel getLastDay() {

		if (getDay().size() < 2) {
			return null;
		}

		return (EventDayModel) getDay().get(getDay().size() - 1);

	}

	/**
	 * Returns day period between first and last date.
	 *
	 * @return day period
	 *
	 * @since 0.15.0
	 */
	public long getDayPeriod() {

		if (getFirstDay() == null) {
			return -1;
		}

		if (getLastDay() == null) {
			return 1;
		}

		return ChronoUnit.DAYS.between((LocalDate) getLastDay().getDate().getValue(), (LocalDate) getFirstDay().getDate().getValue());

	}


	/**
	 * Returns list of referees for highest assignment type.
	 *
	 * @return list of referees for highest assignment type
	 *
	 * @since 0.17.0
	 */
	public List<Referee> getRefereesForHighestAssignmentType() {

		return getRefereesFor(((ContentModel) AppModel.getData().getContent()).getHighestAssignmentType());

	}

	/**
	 * Returns sorted list of referees for given assignment type.
	 *
	 * @param theRefereeAssignmentType assignment type
	 * @return list of referees for given assignment type
	 *
	 * @since 0.17.0
	 */
	public List<Referee> getRefereesFor(
			final RefereeAssignmentType theRefereeAssignmentType
			) {

		List<Referee> lstReturn = new ArrayList<>();

		getDay().stream().forEach(eventday -> {
			lstReturn.addAll(((EventDayModel) eventday).getRefereesFor(theRefereeAssignmentType));
		});

		return lstReturn.stream()
				.distinct()
				.sorted(PersonModel.DEFAULT_COMPARATOR)
				.collect(Collectors.toUnmodifiableList());

	}

	/**
	 * Returns referee text.
	 *
	 * @return referee text
	 *
	 * @since 0.17.0
	 */
	public SimpleStringProperty getRefereeText() {

		return new SimpleStringProperty(
				getRefereesForHighestAssignmentType().stream()
						.map(referee -> referee.getDisplayText().getValueSafe())
						.collect(Collectors.joining(", "))
				);

	}

	/**
	 * Is event canceled?
	 *
	 * @return is canceled?
	 *
	 * @since 0.17.0
	 */
	@Override
	public SimpleBooleanProperty getCanceled() {

		return new SimpleBooleanProperty((super.getCanceled() != null) && super.getCanceled().getValue());

	}

	/**
	 * Is event canceled?
	 *
	 * @return is canceled?
	 *
	 * @since 0.17.0
	 */
	public boolean isCanceled() {

		return getCanceled().getValue();

	}

	/**
	 * Returns number of missing assignments for given assignment type.
	 *
	 * @param theRefereeAssignmentType assignment type
	 * @return number of missing assignments for given assignment type
	 *
	 * @since 0.18.0
	 */
	public int getMissingCountFor(
			final RefereeAssignmentType theRefereeAssignmentType
			) {

		int iMissingCount = 0;

		for (EventDay theEventDay : getDay()) {
			iMissingCount = Math.max(iMissingCount, ((EventDayModel) theEventDay).getMissingCountFor(theRefereeAssignmentType));
		}

		return iMissingCount;

	}

}

/* EOF */
