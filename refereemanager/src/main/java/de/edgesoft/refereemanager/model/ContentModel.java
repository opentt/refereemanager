package de.edgesoft.refereemanager.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlTransient;

import de.edgesoft.edgeutils.commons.ext.ModelClassExt;
import de.edgesoft.refereemanager.jaxb.Club;
import de.edgesoft.refereemanager.jaxb.ContactType;
import de.edgesoft.refereemanager.jaxb.Content;
import de.edgesoft.refereemanager.jaxb.EventDateType;
import de.edgesoft.refereemanager.jaxb.League;
import de.edgesoft.refereemanager.jaxb.LeagueGame;
import de.edgesoft.refereemanager.jaxb.OtherEvent;
import de.edgesoft.refereemanager.jaxb.Person;
import de.edgesoft.refereemanager.jaxb.Referee;
import de.edgesoft.refereemanager.jaxb.RefereeAssignmentType;
import de.edgesoft.refereemanager.jaxb.RoleType;
import de.edgesoft.refereemanager.jaxb.SexType;
import de.edgesoft.refereemanager.jaxb.StatusType;
import de.edgesoft.refereemanager.jaxb.Team;
import de.edgesoft.refereemanager.jaxb.Tournament;
import de.edgesoft.refereemanager.jaxb.Trainee;
import de.edgesoft.refereemanager.jaxb.TrainingLevelType;
import de.edgesoft.refereemanager.jaxb.Venue;
import de.edgesoft.refereemanager.jaxb.Wish;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Content model, additional methods for jaxb model class.
 *
 * ## Legal stuff
 *
 * Copyright 2016-2020 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * This file is part of Open-TT: Referee Manager.
 *
 * Open-TT: Referee Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Open-TT: Referee Manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-TT: Referee Manager. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Ekkart Kleinod
 * @since 0.5.0
 */
public class ContentModel extends Content {

	/**
	 * Observable list of referees (singleton).
	 *
	 * @since 0.10.0
	 */
	@XmlTransient
	private ObservableList<Referee> observableReferees = null;

	/**
	 * Observable list of trainees (singleton).
	 *
	 * @since 0.12.0
	 */
	@XmlTransient
	private ObservableList<Trainee> observableTrainees = null;

	/**
	 * Observable list of people (singleton).
	 *
	 * @since 0.12.0
	 */
	@XmlTransient
	private ObservableList<Person> observablePeople = null;

	/**
	 * Observable list of leagues (singleton).
	 *
	 * @since 0.15.0
	 */
	@XmlTransient
	private ObservableList<League> observableLeagues = null;

	/**
	 * Observable list of venues (singleton).
	 *
	 * @since 0.15.0
	 */
	@XmlTransient
	private ObservableList<Venue> observableVenues = null;

	/**
	 * Observable list of sex types (singleton).
	 *
	 * @since 0.13.0
	 */
	@XmlTransient
	private ObservableList<SexType> observableSexTypes = null;

	/**
	 * Observable list of event date types (singleton).
	 *
	 * @since 0.17.0
	 */
	@XmlTransient
	private ObservableList<EventDateType> observableEventDateTypes = null;

	/**
	 * Observable list of role types (singleton).
	 *
	 * @since 0.15.0
	 */
	@XmlTransient
	private ObservableList<RoleType> observableRoleTypes = null;

	/**
	 * Observable list of contact types (singleton).
	 *
	 * @since 0.15.0
	 */
	@XmlTransient
	private ObservableList<ContactType> observableContactTypes = null;

	/**
	 * Observable list of status types (singleton).
	 *
	 * @since 0.15.0
	 */
	@XmlTransient
	private ObservableList<StatusType> observableStatusTypes = null;

	/**
	 * Observable list of training level types (singleton).
	 *
	 * @since 0.15.0
	 */
	@XmlTransient
	private ObservableList<TrainingLevelType> observableTrainingLevelTypes = null;

	/**
	 * Observable list of referee assignment types (singleton).
	 *
	 * @since 0.17.0
	 */
	@XmlTransient
	private ObservableList<RefereeAssignmentType> observableRefereeAssignmentTypes = null;

	/**
	 * Observable list of league games (singleton).
	 *
	 * @since 0.15.0
	 */
	@XmlTransient
	private ObservableList<LeagueGame> observableLeagueGames = null;

	/**
	 * Observable list of tournaments (singleton).
	 *
	 * @since 0.15.0
	 */
	@XmlTransient
	private ObservableList<Tournament> observableTournaments = null;

	/**
	 * Observable list of other events (singleton).
	 *
	 * @since 0.15.0
	 */
	@XmlTransient
	private ObservableList<OtherEvent> observableOtherEvents = null;

	/**
	 * Observable list of clubs (singleton).
	 *
	 * @since 0.15.0
	 */
	@XmlTransient
	private ObservableList<Club> observableClubs = null;


	/**
	 * Returns observable list of referees.
	 *
	 * @return observable list of referees
	 *
	 * @since 0.10.0
	 */
	public ObservableList<Referee> getObservableReferees() {
		if (observableReferees == null) {
			observableReferees = FXCollections.observableList(getReferee());
		}
		return observableReferees;
	}

	/**
	 * Returns observable list of trainees.
	 *
	 * @return observable list of trainees
	 *
	 * @since 0.12.0
	 */
	public ObservableList<Trainee> getObservableTrainees() {
		if (observableTrainees == null) {
			observableTrainees = FXCollections.observableList(getTrainee());
		}
		return observableTrainees;
	}

	/**
	 * Returns observable list of people.
	 *
	 * @return observable list of people
	 *
	 * @since 0.12.0
	 */
	public ObservableList<Person> getObservablePeople() {
		if (observablePeople == null) {
			observablePeople = FXCollections.observableList(getPerson());
		}
		return observablePeople;
	}

	/**
	 * Returns observable list of roles.
	 *
	 * @return observable list of roles
	 *
	 * @since 0.15.0
	 */
	public ObservableList<RoleType> getObservableRoleTypes() {
		if (observableRoleTypes == null) {
			observableRoleTypes = FXCollections.observableList(getRoleType());
		}
		return observableRoleTypes;
	}

	/**
	 * Returns observable list of contact types.
	 *
	 * @return observable list of contact types
	 *
	 * @since 0.15.0
	 */
	public ObservableList<ContactType> getObservableContactTypes() {
		if (observableContactTypes == null) {
			observableContactTypes = FXCollections.observableList(getContactType());
		}
		return observableContactTypes;
	}

	/**
	 * Returns observable list of sex types.
	 *
	 * @return observable list of sex types
	 *
	 * @since 0.13.0
	 */
	public ObservableList<SexType> getObservableSexTypes() {
		if (observableSexTypes == null) {
			observableSexTypes = FXCollections.observableList(getSexType());
		}
		return observableSexTypes;
	}

	/**
	 * Returns observable list of event date types.
	 *
	 * @return observable list of event date types
	 *
	 * @since 0.17.0
	 */
	public ObservableList<EventDateType> getObservableEventDateTypes() {
		if (observableEventDateTypes == null) {
			observableEventDateTypes = FXCollections.observableList(getEventDateType());
		}
		return observableEventDateTypes;
	}

	/**
	 * Returns observable list of leagues.
	 *
	 * @return observable list of leagues
	 *
	 * @since 0.15.0
	 */
	public ObservableList<League> getObservableLeagues() {
		if (observableLeagues == null) {
			observableLeagues = FXCollections.observableList(getLeague());
		}
		return observableLeagues;
	}

	/**
	 * Returns observable list of venues.
	 *
	 * @return observable list of venues
	 *
	 * @since 0.15.0
	 */
	public ObservableList<Venue> getObservableVenues() {
		if (observableVenues == null) {
			observableVenues = FXCollections.observableList(getVenue());
		}
		return observableVenues;
	}

	/**
	 * Returns observable list of status types.
	 *
	 * @return observable list of status types
	 *
	 * @since 0.15.0
	 */
	public ObservableList<StatusType> getObservableStatusTypes() {
		if (observableStatusTypes == null) {
			observableStatusTypes = FXCollections.observableList(getStatusType());
		}
		return observableStatusTypes;
	}

	/**
	 * Returns observable list of training level types.
	 *
	 * @return observable list of training level types
	 *
	 * @since 0.15.0
	 */
	public ObservableList<TrainingLevelType> getObservableTrainingLevelTypes() {
		if (observableTrainingLevelTypes == null) {
			observableTrainingLevelTypes = FXCollections.observableList(getTrainingLevelType());
		}
		return observableTrainingLevelTypes;
	}

	/**
	 * Returns observable list of referee assignment types.
	 *
	 * @return observable list of referee assignment types
	 *
	 * @since 0.17.0
	 */
	public ObservableList<RefereeAssignmentType> getObservableRefereeAssignmentTypes() {
		if (observableRefereeAssignmentTypes == null) {
			observableRefereeAssignmentTypes = FXCollections.observableList(getRefereeAssignmentType());
		}
		return observableRefereeAssignmentTypes;
	}

	/**
	 * Returns observable list of league games.
	 *
	 * @return observable list of league games
	 *
	 * @since 0.15.0
	 */
	public ObservableList<LeagueGame> getObservableLeagueGames() {
		if (observableLeagueGames == null) {
			observableLeagueGames = FXCollections.observableList(getLeagueGame());
		}
		return observableLeagueGames;
	}

	/**
	 * Returns observable list of tournaments.
	 *
	 * @return observable list of tournaments
	 *
	 * @since 0.15.0
	 */
	public ObservableList<Tournament> getObservableTournaments() {
		if (observableTournaments == null) {
			observableTournaments = FXCollections.observableList(getTournament());
		}
		return observableTournaments;
	}

	/**
	 * Returns observable list of other events.
	 *
	 * @return observable list of other events
	 *
	 * @since 0.15.0
	 */
	public ObservableList<OtherEvent> getObservableOtherEvents() {
		if (observableOtherEvents == null) {
			observableOtherEvents = FXCollections.observableList(getOtherEvent());
		}
		return observableOtherEvents;
	}

	/**
	 * Returns observable list of clubs.
	 *
	 * @return observable list of clubs
	 *
	 * @since 0.15.0
	 */
	public ObservableList<Club> getObservableClubs() {
		if (observableClubs == null) {
			observableClubs = FXCollections.observableList(getClub());
		}
		return observableClubs;
	}


	/**
	 * Returns all referenced (used) clubs.
	 *
	 * @return referenced clubs (empty if there are none)
	 *
	 * @since 0.7.0
	 */
	public Set<Club> getReferencedClubs() {

		Set<Club> setReturn = new HashSet<>();

		for (Referee theReferee : getReferee()) {

			for (Wish theWish : theReferee.getPrefer()) {
				if (theWish.getClub() != null) {
					setReturn.add(theWish.getClub());
				}
			}

			for (Wish theWish : theReferee.getAvoid()) {
				if (theWish.getClub() != null) {
					setReturn.add(theWish.getClub());
				}
			}

			if (theReferee.getMember() != null) {
				setReturn.add(theReferee.getMember());
			}

			if (theReferee.getReffor() != null) {
				setReturn.add(theReferee.getReffor());
			}

		}

		return setReturn;

	}

	/**
	 * Returns all non-referenced (unused) clubs.
	 *
	 * @return non-referenced clubs (empty if there are none)
	 *
	 * @since 0.7.0
	 */
	public Set<Club> getNonReferencedClubs() {

		Set<Club> setReturn = new HashSet<>();

		setReturn.addAll(getClub());
		setReturn.removeAll(getReferencedClubs());

		return setReturn;

	}

	/**
	 * Returns all used leagues, i.e. leagues with assignments.
	 *
	 * @todo rewrite to look for assignments
	 *
	 * @return used leagues (empty if there are none)
	 *
	 * @since 0.9.0
	 */
	public List<League> getUsedLeagues() {

		List<League> lstReturn = new ArrayList<>();

		for (League theLeague : getLeague().stream().sorted(LeagueModel.RANK_DISPLAYTITLE).collect(Collectors.toList())) {

			if (theLeague.getNational().get()) {
				lstReturn.add(theLeague);
			}
		}

		return lstReturn;

	}

	/**
	 * Returns all local home teams for a league.
	 *
	 * @todo rewrite to look for assignments
	 *
	 * @param theLeague league
	 * @return used home teams (empty if there are none)
	 *
	 * @since 0.9.0
	 */
	public List<Team> getLocalHomeTeams(final League theLeague) {

		List<Team> lstReturn = new ArrayList<>();

		for (Team theTeam : getTeam().stream().sorted(TitledIDTypeModel.DISPLAYTITLE).collect(Collectors.toList())) {
			if ((theTeam.getLeague() != null) && (theTeam.getLeague() == theLeague)) {
				if ((theTeam.getClub() != null) && (theTeam.getClub().getIsLocal().get())) {
					lstReturn.add(theTeam);
				}
			}
		}

		return lstReturn;

	}

	/**
	 * Sorts all data in model.
	 *
	 * First store data in a temporary list, because of clearing the list before filling it again.
	 *
	 * @since 0.12.0
	 */
	public void sortData() {

		List<Person> lstPerson = getPerson().stream().sorted(PersonModel.NAME_FIRSTNAME).collect(Collectors.toList());
		getPerson().clear();
		getPerson().addAll(lstPerson);

		List<Referee> lstReferee = getReferee().stream().sorted(PersonModel.NAME_FIRSTNAME).collect(Collectors.toList());
		getReferee().clear();
		getReferee().addAll(lstReferee);

		List<Trainee> lstTrainee = getTrainee().stream().sorted(PersonModel.NAME_FIRSTNAME).collect(Collectors.toList());
		getTrainee().clear();
		getTrainee().addAll(lstTrainee);

		List<League> lstLeague = getLeague().stream().sorted(LeagueModel.RANK_DISPLAYTITLE).collect(Collectors.toList());
		getLeague().clear();
		getLeague().addAll(lstLeague);

		List<Club> lstClub = getClub().stream().sorted(TitledIDTypeModel.DISPLAYTITLE).collect(Collectors.toList());
		getClub().clear();
		getClub().addAll(lstClub);

		List<Team> lstTeam = getTeam().stream().sorted(TitledIDTypeModel.DISPLAYTITLE).collect(Collectors.toList());
		getTeam().clear();
		getTeam().addAll(lstTeam);

		List<LeagueGame> lstLeagueGame = getLeagueGame().stream().sorted(EventDateModel.DATE_FIRST).collect(Collectors.toList());
		getLeagueGame().clear();
		getLeagueGame().addAll(lstLeagueGame);

		List<Tournament> lstTournament = getTournament().stream().sorted(EventDateModel.DATE_FIRST).collect(Collectors.toList());
		getTournament().clear();
		getTournament().addAll(lstTournament);

		List<OtherEvent> lstOtherEvent = getOtherEvent().stream().sorted(EventDateModel.DATE_FIRST).collect(Collectors.toList());
		getOtherEvent().clear();
		getOtherEvent().addAll(lstOtherEvent);

		List<Venue> lstVenue = getVenue().stream().sorted(TitledIDTypeModel.DISPLAYTITLE).collect(Collectors.toList());
		getVenue().clear();
		getVenue().addAll(lstVenue);

		List<SexType> lstSexType = getSexType().stream().sorted(TitledIDTypeModel.TITLE).collect(Collectors.toList());
		getSexType().clear();
		getSexType().addAll(lstSexType);

		List<ContactType> lstContactType = getContactType().stream().sorted(TitledIDTypeModel.TITLE).collect(Collectors.toList());
		getContactType().clear();
		getContactType().addAll(lstContactType);

		List<StatusType> lstStatusType = getStatusType().stream().sorted(TitledIDTypeModel.TITLE).collect(Collectors.toList());
		getStatusType().clear();
		getStatusType().addAll(lstStatusType);

		List<RefereeAssignmentType> lstRefereeAssignmentType = getSortedData(getRefereeAssignmentType(), RefereeAssignmentTypeModel.DEFAULT_COMPARATOR);
		getRefereeAssignmentType().clear();
		getRefereeAssignmentType().addAll(lstRefereeAssignmentType);

		List<RoleType> lstPersonRoleType = getRoleType().stream().sorted(TitledIDTypeModel.DISPLAYTITLE).collect(Collectors.toList());
		getRoleType().clear();
		getRoleType().addAll(lstPersonRoleType);

		List<TrainingLevelType> lstTrainingLevelType = getTrainingLevelType().stream().sorted(TrainingLevelTypeModel.RANK).collect(Collectors.toList());
		getTrainingLevelType().clear();
		getTrainingLevelType().addAll(lstTrainingLevelType);

	}

	/**
	 * Returns list of all people (people, referees, trainees).
	 *
	 * @return list of all people
	 *
	 * @since 0.15.0
	 */
	public List<Person> getAllPeople() {

		List<Person> lstReturn = new ArrayList<>();

		lstReturn.addAll(getPerson());
		lstReturn.addAll(getReferee());
		lstReturn.addAll(getTrainee());

		return lstReturn.stream().sorted(PersonModel.NAME_FIRSTNAME).collect(Collectors.toList());

	}

	/**
	 * Returns sorted list of given data.
	 *
	 * @return sorted list
	 *
	 * @since 0.17.0
	 */
	public static <T extends ModelClassExt> List<T> getSortedData(final List<T> theData, Comparator<? super T> theComparator) {
		return theData.stream().sorted(theComparator).collect(Collectors.toList());
	}

	/**
	 * Returns highest assignment type by default comparator.
	 *
	 * @return highest assignment type
	 *
	 * @since 0.17.0
	 */
	public RefereeAssignmentType getHighestAssignmentType() {

		return getRefereeAssignmentType()
				.stream()
				.sorted(RefereeAssignmentTypeModel.DEFAULT_COMPARATOR)
				.findFirst()
				.get();

	}

}

/* EOF */
