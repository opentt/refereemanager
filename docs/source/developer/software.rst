Software
========

.. index:: software

.. highlights::
  What is needed for development?

I develop under :program:`LinuxMint` (:program:`Ubuntu`) and :program:`Windows`.
Installer creation for :program:`Windows` can be done in :program:`Windows` and :program:`LinuxMint`, for :program:`Linux` (:program:`Ubuntu`) in :program:`LinuxMint` only.


Mandatory Software
------------------

Java
^^^^

The RefereeManager is written in :program:`Java 9+`, using :program:`JavaFX`, and :program:`JAXB`.
It uses language features of the newest version available in LinuxMint and supported by all maven plugins.

You can check the current Java version in :file:`pom.xml` of project :file:`refereemanager`.

In https://gitlab.com/open-tt/refereemanager/blob/master/refereemanager/pom.xml look for the line

.. code-block:: xml

	<java.version>...</java.version>


Git
^^^

You need :program:`git` for checking out the code and providing merge requests.


Maven
^^^^^

:program:`Maven` is used for providing the external modules for the code.
If you use :program:`eclipse`, a sufficient maven is mostly included.


Recommended Software
--------------------

Ant
^^^

You need :program:`ant` for execution of the JAXB and release scripts.
You can choose not to use ant, in this case you have to execute the required scripts by hand.


Eclipse
^^^^^^^

The development of the *RefereeManager* takes place in :program:`eclipse`, an eclipse project and launches are provided in the repository.
It is possible to use another development environment, I just cannot give any advice outside eclipse.


Software for Creation of Releases
---------------------------------

dpkg
^^^^

The Debian (Ubuntu) installer is created using :program:`dpkg`.


NSIS
^^^^

The Windows installer is created with :program:`NSIS`.
This is very painful and, at the moment, very unstable.
I am not sure if I stick with NSIS in the future.
