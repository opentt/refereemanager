RefereeManager – The Developer Manual
=====================================

.. image:: /_static/icon-64.*
	:alt: Logo – a yellow and a red card
	:align: right

.. highlights::
	Referee management for table tennis

.. toctree::
	:maxdepth: 2
	:caption: Content:

	structure
	setup
	software
	release
	implementation

The developer manual is written in English, because I program in English.
As I am no native speaker, some phrases might sound German.

.. warning:: I switched to Java 9+ recently, the documentation does not reflect all changes yet.

Source code
	per git in gitlab unter: https://gitlab.com/open-tt/refereemanager

Fehler melden
	in gitlab unter: https://gitlab.com/open-tt/refereemanager/issues

Wünsche melden
	in gitlab unter: https://gitlab.com/open-tt/refereemanager/issues

Übersetzungen
	gebe ich bekannt, wenn es soweit ist, derzeit ist die technische Basis dafür noch nicht da
