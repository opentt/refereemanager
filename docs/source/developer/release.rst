Create a release
================

.. highlights::
  How to create a release.

It is assumed, that all development branches (:file:`feature`) are merged into the :file:`master` branch and the release is created from the :file:`master` branch.
The examples will use release *0.15.0* as example.

If you are not sure about a step, save your progress in :program:`git`, then execute the step.
This way you can check the status with :samp:`git status` and :samp:`git diff {file}` and go back to the last step, if needed, with :samp:`git checkout -- {file}`.

#. create a feature branch for finalizing the release (this differs from the mainline model, but I find it easier to work this way)

   .. code-block:: bash

     repopath $ git checkout -b feature/release-0.15.0
     repopath $

#. update :file:`changelog` and :file:`changelog.md`

   The first file is for debian packages, the latter for humans.
   Maybe in the future there will be a script converting one to the other, but for now redundancy rules.

#. check if :file:`README.md` is up-to-date

#. check if documentation is up-to-date

#. update :file:`refereemanager/src/main/resources/project.properties`

   * version numbers
   * copyright information

#. update version number in :file:`build/build.xml`, as this cannot be done via script

#. goto :file:`build` directory

   .. code-block:: bash

     repopath $ cd build/
     repopath/build $

#. set version and copyright information in all files in directory :file:`refereemanager` via :program:`ant` task

   .. code-block:: bash

     repopath/build $ ant setversion

     Buildfile: repopath/build/build.xml

     clearLog:

     setversion:
       [echo] @version
       [echo] docversion
       [echo] copyright
       [echo] Ready, now create new jar, edit changelog, then call 'sudo ant debinstall' (as root).

     BUILD SUCCESSFUL
     Total time: 1 second

     repopath/build $

#. create new jar

   #. update :file:`refereemanager/pom.xml` with correct version, check other information
   #. in :program:`eclipse` right click :file:`pom.xml`, then select :menuselection:`Run As --> Maven build`

      This calls :program:`maven` with the goals :program:`clean compile package`.
      If you do not use :program:`eclipse`, call :program:`maven` accordingly.
   #. copy the generated jar from :file:`refereemanager/target/refereemanager-{version}.jar` to :file:`files/refereemanager.jar`
   #. remove generated :file:`refereemanager/dependency-reduced-pom.xml`
   #. remove generated :file:`refereemanager/target/` to get rid of old artifacts for further development

#. create :program:`deb` installer files via :program:`ant` task

   .. important:: At this point :file:`changelog` and :file:`changelog.md` have to contain the correct release date (and time), as they are used in the generation steps.

   The task has to be called as *root* in order for *user* and *group* of the files to be correctly set.

   .. code-block:: bash

     repopath/build $ sudo ant debinstall

     Buildfile: repopath/build/build.xml

     clearLog:

     debinstall:
          [copy] Copying 6 files to repopath/build/debian-tmp
          [gzip] Building: repopath/build/debian-tmp/usr/share/doc/refereemanager/changelog.gz
          [echo] Ready, now call 'ant createdeb' (not as root).

     BUILD SUCCESSFUL
     Total time: 1 second

     repopath/build $

#. create deb file via :program:`dpkg` and :program:`ant` task

   .. code-block:: bash

     repopath/build $ ant createdeb

     Buildfile: repopath/build/build.xml

     createdeb:
        [move] Moving 1 file to repopath/files
        [echo] Ready, now call 'sudo ant clearTmp' (as root).

     BUILD SUCCESSFUL
     Total time: 5 seconds

     repopath/build $

#. remove temporary files as *root* via :program:`ant` task

   .. code-block:: bash

     repopath/build $ sudo ant clearTmp

     Buildfile: repopath/build/build.xml

     clearTmp:
      [delete] Deleting directory repopath/build/debian-tmp
        [echo] Ready, now call 'ant wininstall'.

     BUILD SUCCESSFUL
     Total time: 0 seconds

     repopath/build $

#. create :program:`windows` installer via :program:`NSIS` and :program:`ant` task

   .. code-block:: bash

     repopath/build $ ant wininstall

     Buildfile: repopath/build/build.xml

     clearLog:

     wininstall:
          [copy] Copying 1 file to repopath/build/win-tmp

     installer:

     nsis:windows:

     nsis:unix:

     nsis:
          [move] Moving 1 file to repopath/files
        [delete] Deleting directory repopath/build/win-tmp

     BUILD SUCCESSFUL
     Total time: 1 second

     repopath/build $

#. check if all files look good

#. merge feature branch to master without fast forward

   .. code-block:: bash

     repopath $ git checkout master
     ...
     repopath $ git merge --no-ff feature/release-0.15.0
     ...
     repopath $

#. remove feature branch, remote too, if needed

   .. code-block:: bash

     repopath $ git branch -d feature/release-0.15.0
     ...
     repopath $ git push origin --delete feature/release-0.15.0
     ...
     repopath $

#. create release branch, tag release

   .. code-block:: bash

     repopath $ git checkout -b release/0.15
     ...
     repopath $ git tag -a 0.15.0
     ...
     repopath $

#. push everything if you haven't done so yet

   .. code-block:: bash


     repopath $ git push --all
     ...
     repopath $ git push --tags
     ...
     repopath $

#. upload files to sourceforge

   - https://sourceforge.net/projects/refereemanager/files/
