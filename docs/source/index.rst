RefereeManager - Das Handbuch
=============================

.. image:: /_static/icon-64.*
	:alt: Logo - eine gelbe und eine rote Karte
	:align: right

.. highlights::
	Schiedsrichterverwaltung für Tischtennis

.. toctree::
	:maxdepth: 2
	:caption: Inhalte:

	user/index
	developer/index
	todo

Der *RefereeManager* ist eine Schiedsrichterverwaltung für Tischtennisschiedsrichter, von :abbr:`VSR (Verbandsschiedsrichter)` bis :abbr:`IU (International Umpire)`.

Andere Sportarten werden nicht explizit unterstützt, evtl. kann die Software aber auch dafür eingesetzt werden.

Die Anrede in diesem Handbuch ist durchgängig weiblich.
Personen anderen Geschlechts können natürlich verwaltet werden bzw. das Programm bedienen.

.. note:: Das Programm ist noch stark in der Entwicklung.

	Bis zur Version 1 kann sich insbesondere das Datenmodell noch ändern, ohne dass eine automatische Datenübernahme angeboten wird.
