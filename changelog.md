# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- referee meeting time
- new method getMissingCount for dates/days
- more i18n texts

### Fixed

- replaced DatePicker by EdgeDatePicker -> improved manual input of dates
- shorttitle is checked for blank before using it for displaytitle


## [0.17.0] - 2020-06-06

### Added

- documentation (developer, user) via Read the Docs
- settings value for number of decimal places for geolocations
- CSS styling
- I18N support (not complete)
- tournament support
- first idea of batch document generation

### Changed

- complete rewrite of the communication GUI
- new API for freemarker templates
- rewrite of the preferences dialog
- no value for isPrimary in contants means true
- geolocation is part of an address now
- venues are not addresses anymore but contain addresses
- contacts are no titled elements anymore
- many small improvements (see logs)
- data model for tournaments, referees, ...

### Removed

- documentation (developer, user) with mallard

### Fixed

- NullPointerException for referees without status
- getPrimary... methods now working correctly if there is only one contact
- template loading
- sending mails to dead people
- many small bugs (see logs)


## [0.16.0] - 2019-07-20

### Added

- copy button and context menu for emails in overview views of people
- option for scaling icons
- module structure for Java 11+
- started i18n support
- styling with css
- filter for exam results

### Changed

- icons now svg instead of png
- split menu items instead of normal menu items


## [0.15.0] - 2018-08-17

### Added

- league games, not completed yet
- event overview
- overviews and editing
	- clubs
	- leagues
	- venues
	- sex types
	- roles
	- contact types
	- status
	- training type
- icons for all things
- help &rarr; known problems
- installer
	- Windows
	- Linux (deb)
- user manual
- JUnit tests including automated GUI tests
- human readable changelog style (Ticket #44)

### Changed

- moved from github to gitlab
- moved from TT-Schiri to Open-TT project/group
- moved from git branching model to stable mainline model (Ticket #43)
- layouts and tabbed edit dialogs
- data model as seen fit for data and GUI generation
- export API to freemarker
- GPL instead of LGPL (always was GPL, but there were misleading texts)
- developer documentation now with ducktype

### Removed

- DBOperations class


## [0.14.0] - 2017-07-16

### Added

- editing of people, referees, and trainees
- logo and splash screen
- win and debian installer

### Changed

- small model changes

### Fixed

- bugfixing in communication panel


## [0.13.0] - 2017-03-14

### Added

- basic editing features

### Changed

- rewrite docsByLetter
- improved overview


## [0.12.0] - 2017-02-07

### Added

- trainees

### Changed

- communication and document generation using Freemarker templates, not own template language
- improved referee lists


## [0.11.0] - 2016-11-24

- Open-TT -> TT-Schiri


## [0.10.0] - 2016-11-23

- before restructuring to TT-Schiri

### Changed

- communication
- code clearance


## [0.9.0] - 2016-10-25

- saving progress before massive cleanup and graphical interface

### Added

- website exports
- first assignments, tournaments and venues
- no predefined templates


## [0.8.0] - 2016-08-23

### Added

- referee communication by mail and letter

### Changed

- improvement of database operations
- refined template helper functions and template language


## [0.7.0] - 2016-08-01

### Added

- database operations: sort and remove unused clubs


## [0.6.0] - 2016-07-31

### Added

- export of contact and disposition list in mmd
- first draft of template language


## [0.5.0] - 2016-07-27

- start of Java development

### Added

- working java backend
- export of anonymous referee list in mmd

### Changed

- many improvements and changes in data modeling

### Removed

- PHP code


## [0.4.0] - 2016-05-25

- saving progress of PHP and database modeling
- giving up web based solution because of complexity of database and PHP
- switch to NoSQL data (XML) and java backend with simplified PHP frontend or static pages

### Added

- converter database -> XML

### Changed

- switched to CakePHP 3.*
- extensive rewrite of database and functions


## [0.3.0] - 2015-07-20

- last version before switching to CakePHP 3.*

### Added

- many functions working for referees and messages


## [0.2.0] - 2014-05-04

### Added

- output of referee lists: HTML, PDF, Excel

### Fixed

- database for referees working



## [0.1.0] - 2013-10-13

- saving the code before introducing the new branching model

### Added

- first functionality
